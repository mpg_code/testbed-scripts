#!/usr/bin/env python

for num in [2**n for n in xrange(0, 10)]:
	for queue in ("bfifo", "pfifo", "sfq", "sfb", "codel"):
		for retr in xrange(1,7):
			for stream in ('greedy', 'thin'):
				with open('%dvs%d-%s--%s-%dretr-aggr.dat' % (num,num,queue,stream,retr)) as f:
					print "%s\t%dvs%d\t%s\t%d. retr:" % (stream,num,num,queue,retr), len(f.readlines())

		print '----'
		for stream in ('greedy', 'thin'):
			with open('%dvs%d-%s--%s-all-aggr.dat' % (num,num,queue,stream)) as f:
				print "%s\t%dvs%d\t%s\tall:" % (stream,num,num,queue), len(f.readlines())
		print ''

