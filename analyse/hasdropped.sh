#!/bin/bash

files=$(find . -regextype posix-extended -regex "^\./fair-stable-[a-z]+-[a-z]+-[0-9]+s-[0-9]+vs[0-9]+-streams-iat[0-9]+-ps[0-9]+-(|-a|-m|-am)\.log")

for file in $files; do
	num=$(grep -hoE "[0-9]+ packets dropped" $file | grep -oE "[0-9]+")

	if [ "$num" -gt 0 ]; then
		echo $file $num
	fi
done
