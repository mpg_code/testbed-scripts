#!/bin/bash

analyse_tcp=/home/jonas/master/tstools_analysetcp/build/analyseTCP

for f in $(ls *.dump)
do
    echo "processing $f"
    vs=$(echo $f | cut -d"-" -f6)
    tcp_var=$(echo $f | grep -o -E "streams-.*" | cut -d"-" -f5)
    queue=$(echo $f | grep -o -E "(pfifo|bfifo|sfq|sfb|codel)")
    # for thin streams
    $analyse_tcp -b -f $f -s 10.0.0.10 -u ${vs}-${queue}-${tcp_var}-thin -p 23456 > ${vs}-${queue}-${tcp_var}-thin.log
    # greedy streams
    $analyse_tcp -b -f $f -s 10.0.0.10 -u ${vs}-${queue}-${tcp_var}-greedy -p 12345 > ${vs}-${queue}-${tcp_var}-greedy.log
done
