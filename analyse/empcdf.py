#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.backends.backend_pdf import PdfPages
from sys import stdout

nums = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512]
queues = ['pfifo', 'bfifo', 'sfq', 'sfb', 'codel']
res = 1000000
lim = 10000


def output(s):
	stdout.write(s)
	stdout.flush()


def discrete_cdf(array, point):
	return array.searchsorted(point, side="left") / float(array.size)


def read_latencies(num_streams, queue, stream_type):
	filename = "%dvs%d-%s--%s-all-aggr.dat" % (num_streams, num_streams, queue, stream_type)
	output( 'processing %s...' % filename )

	with open(filename) as f:
		latencies = [np.float32(l) for l in f]

	output('done\n')
	return np.array(latencies, dtype=np.float32)


def plot_hist(ax, latencies, num, queue, stream_type):
	global lim, res
	max_value = latencies.max()
	output('plotting histogram for %s...' % stream_type)
	ax.set_title('%dvs%d %s %s' % (num, num, queue, stream_type))
	ax.set_ylim(0, latencies.size)
	ax.set_xlim(0, min(max_value, lim) if lim > 0 else max_value)
	ax.set_yticks(np.arange(0, latencies.size + latencies.size * .2, latencies.size * .2))
	ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda value, position: value / latencies.size))
	ax.hist(latencies, res, histtype='step', cumulative=True)
	output('done\n')



pp = PdfPages('cdf.pdf')
for num in nums:
	for queue in queues:
		#plt.close('all')
		#fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(nrows=2, ncols=2)
		fig, ((ax1), (ax3)) = plt.subplots(nrows=2, ncols=1)
		plot_hist(ax1, read_latencies(num, queue, 'thin'), num, queue, 'thin')
		plot_hist(ax3, read_latencies(num, queue, 'greedy'), num, queue, 'greedy')

#		output('plotting discrete cdf for thin...')
#		latencies_thin.sort()
#		x_values = np.arange(0, min(latencies_thin[-1], lim) if lim > 0 else latencies_thin[-1])
#		ax2.set_title('%dvs%d %s thin (discrete)' % (num, num, queue))
#		ax2.plot(x_values, [discrete_cdf(latencies_thin, point) for point in x_values])
#		output('done\n')
#
#		output('plotting discrete cdf for greedy...')
#		latencies_greedy.sort()
#		x_values = np.arange(0, min(latencies_greedy[-1], lim) if lim > 0 else latencies_greedy[-1])
#		ax4.set_title('%dvs%d %s greedy (discrete)' % (num, num, queue))
#		ax4.plot(x_values, [discrete_cdf(latencies_greedy, point) for point in x_values])
#		output('done\n')

		plt.tight_layout()
		#plt.savefig("cdf-%d-%s.pdf" % (num, queue))
		plt.savefig(pp, format='pdf')
pp.close()
