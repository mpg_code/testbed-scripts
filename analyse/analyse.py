#!/usr/bin/env python
import rpy2
from rpy2 import robjects
from rpy2.robjects import r
from rpy2.robjects.vectors import DataFrame, IntVector, FloatVector
import os
import re

#path_to_analysetcp = '~/master/tstools_analysetcp/build/analyseTCP'
path_to_analysetcp = 'analyseTCP'
mechanism_map = {'': "TCP", '-a': "MFR", '-m': "LT", '-am': "LT+MFR"}


class stream_class(object):
	def __init__(self, directory, stream_cfg, result_files):
		self.name = stream_cfg['name']
		self.number = stream_cfg['number']
		self.sndr_side = result_files[0]
		self.recv_side = result_files[1]
		self._config = stream_cfg
		self._dir = directory
		self.ramp_up = stream_cfg['ramp_up_str']

		if 'bandwidth' in stream_cfg:
			self.bandwidth = stream_cfg['bandwidth']
		else:
			self.packet_itt = stream_cfg['packet_itt_str']
			self.packet_size = stream_cfg['packet_size_str']

	@property
	def cmd(self):
		cmd = "%(analysetcp)s -s %(send_addr)s -r %(recv_addr)s -q %(first_port)d-%(last_port)d -f %(strace)s -g %(rtrace)s " % {
				'analysetcp': path_to_analysetcp,
				'send_addr': self._config['send_addr'], 'recv_addr': self._config['recv_addr'],
				'first_port': self._config['start_port'], 'last_port': (self._config['start_port'] + self._config['number']),
				'strace': os.path.join(self._dir, self.sndr_side), 'rtrace': os.path.join(self._dir, self.recv_side)
				}
		return cmd

	@property
	def is_thin(self):
		return not hasattr(self, 'bandwidth')


class test(object):
	def __init__(self, prefix, directory, test_cfg):
		self._pre = prefix
		self._dir = directory
		self.test_number = test_cfg['test']
		self.streams = [stream_class(directory, stream_info['config'], stream_info['results']) for stream_info in test_cfg['streams']]
		self._config = test_cfg['config']
		self.bandwidth = self._config['bw']
		self.rtt = self._config['rtt']
		self.duration = self._config['duration']

	def short_loss_summary(self, allow_color=True):
		regex = re.compile(r'\s+([0-9\.]+)(?:\s+%)?')
		first = True

		for stream in self.streams:
			output_file = os.path.join(self._dir, "%s_%d_%d_%s.txt" % (self._pre, self.test_number, stream._config['recv_port'], stream.name))
			os.system('echo "%s" -e > %s' % (stream.cmd, output_file))
			os.system(stream.cmd + "-e 1>> %s" % output_file)

			data = []
			with open(output_file) as f:
				for line in f.readlines():
					dataline = line.split(':')
					if len(dataline) == 2:
						linedata = re.findall(regex, dataline[1])
						if len(linedata) == 7:
							data.append(linedata)

			df = DataFrame({
				'Packet loss': FloatVector([float(d[5]) for d in data]),
				'Byte loss': FloatVector([float(d[6]) for d in data]),
				})

			colored = False
			summary = r("summary")(df)

			for i in xrange(0, summary.ncol):
				cols = summary.rx(True, i+1)

				f = float(cols[0].rsplit(":", 1)[1].strip())
				l = float(cols[-1].rsplit(":", 1)[1].strip())
				if l - f > 1.5:
					colored = True


			if colored:
				print "\033[1;31m",
			else:
				print "",

			if first:
				print "%2s %6s %6s %6s %6s  " % (str(self.test_number), " ", " ", " ", " "),
				for i in xrange(0, summary.ncol):
					if i > 0:
						print " " * 56 * i,
					print "%-10s" % summary.colnames[i].strip(),
				print

				print "",

				print "%2s %6s %6s %6s %6s |" % ("#", "type", "R-UP", "ITT", "PS"),
				for i in xrange(0, summary.ncol):
					for col in summary.rx(True, i+1):
						print "%10s" % col.rsplit(":", 1)[0].strip(),
					print "|",
				print

				print "",

			if stream.is_thin:
				print "%2d %6s %6s %6s %6s |" % (stream.number, "thin", stream.ramp_up, stream.packet_itt, stream.packet_size),
			else:
				print "%2d %6s %6s %6s %6s |" % (stream.number, "greedy", stream.ramp_up, "N/A", "N/A"),

			for i in xrange(1, summary.ncol + 1):
				for col in summary.rx(True, i):
					print "%10s" % col.rsplit(":", 1)[1].strip(),
				print "|",
			if colored:
				print "\033[0m"
			else:
				print

			first = False


	def loss_summary(self):
		regex = re.compile(r'\s+(\d+\.?\d*)(?![\-\.\d\n]+)')
		for stream_type in self.streams:
			output_file = os.path.join(self._dir, "%s_%d_%d_%s.txt" % (self._pre, self.test_number, stream_type._config['recv_port'], stream_type.name))
			os.system('echo "%s" -e > %s' % (stream_type.cmd, output_file))
			os.system(stream_type.cmd + "-e 1>> %s" % output_file)

			with open(output_file) as f:
				data = []
				for line in f.readlines():
					linedata = re.findall(regex, line)
					if len(linedata) == 6:
						data.append(linedata)

			print output_file
			df = DataFrame({
				'Duration': IntVector([int(d[0]) for d in data]),
				'Packets sent': IntVector([int(d[1]) for d in data]),
				'Packets received': IntVector([int(d[2]) for d in data]),
				'Packet loss': FloatVector([float(d[3]) for d in data]),
				'Byte loss': FloatVector([float(d[4]) for d in data]),
				'Range loss': FloatVector([float(d[5]) for d in data]),
				})

			print "%d streams" % stream_type.number, "thin" if stream_type.is_thin else "greedy",
			if stream_type.is_thin:
				print "itt %s ms" % stream_type.packet_itt, "packet size %s bytes" % stream_type.packet_size, "rampup %s ms" % stream_type.ramp_up,
			print ""
			print r["summary"](df)



class test_suite(object):
	def __init__(self, directory, filename):
		prefix = re.search(r'^(\d+_\d+).*', filename.rsplit(os.path.sep, 1)[1]).groups()[0]

		with open(filename) as f:
			self.tests = [test(prefix, directory, eval(line)) for line in f.readlines()]

		self.differing_fields = set()
		self.find_differences()

	def find_differences(self):
		self.differing_fields = set()

		for test1 in self.tests:
			for test2 in self.tests:
				for key in test1._config.iterkeys():
					if test1._config[key] != test2._config[key]:
						self.differing_fields.add(key)

	def loss_summary(self):
		for i, test in enumerate(self.tests):

			#print ""
			#print "test %d" % test.test_number, "duration %d s" % test.duration, "bandwidth: %d kbit" % test.bandwidth, "rtt: %d ms" % test.rtt
			#for key in self.differing_fields:
			#	print test._config[key],
			#print ""
			#print ""

			test.short_loss_summary()
			if i < len(self.tests) - 1:
				#print "-" * 79
				print "-" * 168


if __name__ == '__main__':
	import glob
	import sys

	if len(sys.argv) != 2:
		print "Missing directory"
		print "Usage: %s <directory>" % sys.argv[0]
		sys.exit(1)

	files = glob.glob(sys.argv[1] + os.path.sep + "*_config.txt")

	if len(files) < 1:
		print "Could not find any config files in directory"
		sys.exit(1)

	test_suites = [test_suite(sys.argv[1], filename) for filename in files]

	for i, suite in enumerate(test_suites):
		suite.loss_summary()
		if i < len(test_suites) - 1:
			print "=" * 79
