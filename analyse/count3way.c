/* compile with -lpcap */
#include <pcap.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <limits.h>
#include <getopt.h>

#define ETH_FRAME_LEN 14

typedef struct {
	uint32_t src_addr;
	uint32_t dst_addr;
	uint16_t src_port;
	uint16_t dst_port;
	
	uint32_t seq_num;
	uint32_t ack_num;
	enum {NONE,SYN,SYNACK,ACK,FIN} state;
	struct timeval established;
} conn_t;

typedef struct tblent {
	conn_t conn;
	struct tblent *next;
} tblent_t;

tblent_t *conntbl;

inline tblent_t* find_conn(uint32_t saddr, uint16_t sport, uint32_t daddr, uint16_t dport)
{
	tblent_t *ptr = conntbl;

	while (ptr != NULL) {
		if (
				(ptr->conn.src_addr == saddr && ptr->conn.dst_addr == daddr && ptr->conn.src_port == sport && ptr->conn.dst_port == dport)
				|| (ptr->conn.src_addr == daddr && ptr->conn.dst_addr == saddr && ptr->conn.src_port == dport && ptr->conn.dst_port == sport)
		   )
			return ptr;

		else
			ptr = ptr->next;
	}

	return ptr;
}

tblent_t* create_conn(uint32_t saddr, uint16_t sport, uint32_t daddr, uint16_t dport)
{
	tblent_t *ptr = conntbl;

	if (conntbl == NULL) {
		conntbl = malloc(sizeof(tblent_t));
		conntbl->conn.src_addr = saddr;
		conntbl->conn.dst_addr = daddr;
		conntbl->conn.src_port = sport;
		conntbl->conn.dst_port = dport;
		conntbl->conn.state = NONE;
		conntbl->next = NULL;
		return conntbl;
	}

	while (ptr->next != NULL)
		ptr = ptr->next;

	ptr->next = malloc(sizeof(tblent_t));
	ptr = ptr->next;
	ptr->conn.src_addr = saddr;
	ptr->conn.dst_addr = daddr;
	ptr->conn.src_port = sport;
	ptr->conn.dst_port = dport;
	ptr->conn.state = NONE;
	ptr->next = NULL;

	return ptr;
}


/* Count the number of flows */
int main(int argc, char **argv)
{
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t *handle;	
	struct pcap_pkthdr *hdr;
	const u_char *pkt;
	uint32_t num_acks, num_synacks, num_syns, num_fins, num_conns;
	tblent_t *ptr;

	uint32_t tcp_offset, src_addr, dst_addr, ack, seq;
	uint16_t src_port, dst_port;
	uint8_t flags;
	struct timeval first_ts = { LONG_MAX, LONG_MAX };
	struct timeval result;
	char *type;
	int verbosity = 1;
	char source[4+4+4+4+5];
	char destin[4+4+4+4+5];

	int opt;
	while ((opt = getopt(argc, argv, ":vq")) != -1) {
		switch (opt) {
			case 'q':
				verbosity = 0;
				break;

			case 'v':
				verbosity = 2;
				break;
		}
	}

	if (optind < 1) {
		fprintf(stderr, "Usage: %s [-v] <capture-file>\n", argv[0]);
		exit(1);
	}

	if ((handle = pcap_open_offline(argv[optind], errbuf)) == NULL) {
		fprintf(stderr, "%s\n", errbuf);
		exit(1);
	}

	conntbl = NULL;
	num_acks = num_syns = num_synacks = num_fins = num_conns = 0;
	
	if (verbosity >= 1) {
		fprintf(stdout, "%-11s\t%-21s\t%-21s\t%-11s\tSEQNO, ACKNO\n", "TIME", "SOURCE", "DESTINATION", "STATE");
	}

	while (pcap_next_ex(handle, &hdr, &pkt) > 0) {
		if (timercmp(&hdr->ts, &first_ts, <))
			first_ts = hdr->ts;

		if ((hdr->len >= ETH_FRAME_LEN + 20 + 20)
				&& ((*((uint8_t*) (pkt+ETH_FRAME_LEN)) & 0xf0) >> 4) == 4 // IPv4?
				&& *((uint8_t*) (pkt+ETH_FRAME_LEN+9)) == 6 ) // IP proto = TCP ?
		{
			tcp_offset = (*((uint8_t*) (pkt+ETH_FRAME_LEN)) & 0x0f) * 4; // IP header size (offset to IP payload / TCP header)
		
			// source host
			src_addr = ntohl(*((uint32_t*) (pkt+ETH_FRAME_LEN+12)));
			src_port = ntohs(*((uint16_t*) (pkt+ETH_FRAME_LEN+tcp_offset)));

			// destination host
			dst_addr = ntohl(*((uint32_t*) (pkt+ETH_FRAME_LEN+16)));
			dst_port = ntohs(*((uint16_t*) (pkt+ETH_FRAME_LEN+tcp_offset+2)));
			
			// sequence number and acknowledgement number
			seq = ntohl(*((uint32_t*) (pkt+ETH_FRAME_LEN+tcp_offset+4)));
			ack = ntohl(*((uint32_t*) (pkt+ETH_FRAME_LEN+tcp_offset+8)));

			// header flags
			flags = *((uint8_t*) (pkt + ETH_FRAME_LEN + tcp_offset + 13));

			/* get connection */
			ptr = find_conn(src_addr, src_port, dst_addr, dst_port);

			if ((flags & 0x3)) {
				type = "ERROR";

				if ((flags & 0x12) == 0x12) {
					++num_synacks;
					type = "SYN-ACK";

					if (ptr == NULL)
						ptr = create_conn(dst_addr, dst_port, src_addr, src_port);
					ptr->conn.state = SYNACK;
					ptr->conn.seq_num = seq;
				} else if ((flags & 0x02) == 0x02) {
					++num_syns;
					type = "SYN";

					if (ptr == NULL)
						ptr = create_conn(src_addr, src_port, dst_addr, dst_port);
					ptr->conn.state = SYN;
				} else if ((flags & 0x01) == 0x01) {
					type = "FIN";
					++num_fins;
				} 

				if (verbosity >= 2) {
					
					sprintf(source, "%u.%u.%u.%u:%u", 
							(src_addr >> 24) & 255,
							(src_addr >> 16) & 255,
							(src_addr >> 8) & 255,
							(src_addr) & 255,
							src_port);
					sprintf(destin, "%u.%u.%u.%u:%u", 
							(dst_addr >> 24) & 255,
							(dst_addr >> 16) & 255,
							(dst_addr >> 8) & 255,
							(dst_addr) & 255,
							dst_port);

					timersub(&hdr->ts, &first_ts, &result);
					fprintf(stdout, "%04ld:%06ld\t%-21s\t%-21s\t%-11s \t%u, %u\n",
							result.tv_sec, result.tv_usec, source, destin, type, seq, ack);
				}
			} else {

				if (ptr != NULL) {
					if (ptr->conn.state == SYNACK && ack == ptr->conn.seq_num + 1) {
						ptr->conn.state = ACK;
						ptr->conn.established = hdr->ts;
						++num_conns;
						++num_acks;

						if (verbosity >= 1) {
							sprintf(source, "%u.%u.%u.%u:%u", 
									(src_addr >> 24) & 255,
									(src_addr >> 16) & 255,
									(src_addr >> 8) & 255,
									(src_addr) & 255,
									src_port);
							sprintf(destin, "%u.%u.%u.%u:%u", 
									(dst_addr >> 24) & 255,
									(dst_addr >> 16) & 255,
									(dst_addr >> 8) & 255,
									(dst_addr) & 255,
									dst_port);

							timersub(&(ptr->conn.established), &first_ts, &result);
							fprintf(stdout, "%04ld:%06ld\t%-21s\t%-21s\t%-11s \t%u, %u\n",
									result.tv_sec, result.tv_usec, source, destin, "ESTABLISHED", seq, ack);
						}
					} else if (ptr->conn.state == SYN) {
						fprintf(stdout, "Lost a SYN-ACK somewhere\n");
					}
				} else {
					fprintf(stdout, "Packet from unregistered connection\n");
				}
			}

		}
	}

//	fprintf(stdout, "Total number of SYNs       : %u\n", num_syns);
//	fprintf(stdout, "Total number of SYN-ACKs   : %u\n", num_synacks);
//	fprintf(stdout, "Total number of ACKs       : %u\n", num_acks);
//	fprintf(stdout, "Total number of FINs       : %u\n", num_fins);
//	fprintf(stdout, "Total number of connections: %u\n", num_conns);

	ptr = conntbl; 
	num_syns = num_synacks = num_conns = 0;
	while (ptr != NULL) {
		
		num_syns += ptr->conn.state == SYN;
		num_synacks += ptr->conn.state == SYNACK;
		num_conns += ptr->conn.state == ACK;

#if 1
		sprintf(source, "%u.%u.%u.%u:%u", 
				(ptr->conn.src_addr >> 24) & 255,
				(ptr->conn.src_addr >> 16) & 255,
				(ptr->conn.src_addr >> 8) & 255,
				(ptr->conn.src_addr) & 255,
				ptr->conn.src_port);
		sprintf(destin, "%u.%u.%u.%u:%u", 
				(ptr->conn.dst_addr >> 24) & 255,
				(ptr->conn.dst_addr >> 16) & 255,
				(ptr->conn.dst_addr >> 8) & 255,
				(ptr->conn.dst_addr) & 255,
				ptr->conn.dst_port);

		fprintf(stdout, "%-21s\t%-21s\n", source, destin);
#endif

		ptr = ptr->next;
		free(conntbl);
		conntbl = ptr;
	}
//	fprintf(stdout, "Unestablished connections  : %u\n", num_syns + num_synacks);

	pcap_close(handle);
	exit(0);
}
