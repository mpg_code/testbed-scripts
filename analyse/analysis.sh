#!/bin/bash

force=0
config="config"		# Test configuration summary file
error="error"		# List of traces that couldn't be analysed
sample_sec=1		# Aggregate interval for tcp-throughput

### Test parameters not extractable from trace filename ###
stream_sender_ip="10.0.0.10"	# streamzero client/sender
stream_receiver_ip="10.0.1.10"	# streamzero server/receiver
stream_thin_remote_port=23456	# Thin streams starting port (server/receiver)
stream_thin_local_port=20000	# Thin streams starting port (client/sender)
stream_greedy_remote_port=12345	# Greedy streams starting port (server/receiver)
stream_greedy_local_port=15000	# Greedy streams starting port (client/sender)
### End ###

# Analysis programs
tput=/home/jonas/master/tput/tput
tcpthroughput=/home/jonas/master/tstools_tcp-throughput/tcp-throughput
analysetcp=/home/jonas/master/tstools_analysetcp/build/analyseTCP
tcptrace=/usr/bin/tcptrace


reset='\e[0m'
err='\e[1;91m'
warning='\e[0;93m'
info=''

# Detect potential problems with a trace dump
# Usage: check_trace_log <tracefile> <number of streams> 
check_trace_log()
{
	args=$1
	num_streams=$2
	tracefile=$1
	basename=$(echo $tracefile | sed -e "s/\.dump//g")

	# Check trace log files for number of packets received in filter
	captured=$(grep -hoE "[0-9]+ packets received by filter" ${basename}.log | grep -oE "[0-9]+")
	let limit=500*num_streams
	if [ "$captured" -lt $limit ]; then
		printf "${err}%s${reset}: Low number of packets captured in trace: %d\n" "${args[@]}" $captured
		return 2
	fi

	# Check trace log files for number of packets dropped by kernel
	fail=0
	dropped=$(grep -hoE "[0-9]+ packets dropped" ${basename}.log | grep -oE "[0-9]+")
	if [ "$dropped" -gt 0 ]; then
		printf "${warning}%s${reset}: Trace has %d dropped packets\n" "${args[@]}" $dropped
		fail=1
	fi

	return $fail
}


# Detect potential problems with streams
# Usage: check_streamzero_log <tracefile> <remote start port> 
check_streamzero_log()
{
	basename=$(echo $1 | sed -e "s/\.dump//g")
	start_port=$2

	stream_type=''

	if (( start_port == stream_thin_remote_port )); then
		stream_type='thin'
	elif (( start_port == stream_greedy_remote_port )); then
		stream_type='greedy'
	else
		return 3
	fi

	args="$1"

	# Check if threads completed successfully
	status=($(grep -hoE "([0-9]+) threads out of ([0-9]+) targeted arrived: ([0-9]+) completed successfully, ([0-9]+) errors and ([0-9]+) aborted" "${basename}_sender_${stream_type}.log" | grep -hoE "([0-9]+)"))
	num_streams=${status[1]}

	# Fatal errors
	if [ ! -f "${basename}_sender_${stream_type}.log" ] || [ ! -f "${basename}_receiver_${stream_type}.log" ]; then
		printf "${err}%s${reset}: streamzero logs not found!!\n" "${args[@]}" 
		return 3
	elif [ -z "${num_streams}" ]; then
		printf "${err}%s${reset}: Incomplete streamzero logs\n" "${args[@]}"
		return 3
	fi

	# Check if any streams failed to connect
	failed=$(grep -hE "No route to host" "${basename}_sender_${stream_type}.log" | wc -l)
	if [ "$failed" -gt 0 ]; then
		printf "${err}%s${reset}: %d streams failed to connect\n" "${args[@]}" $failed
		return 2
	fi

	# Non fatal-errors
	fail=0

	# Count connections received on receiver
	established=$(grep -hE "Conn [0-9]+: New connection" "${basename}_receiver_${stream_type}.log" | wc -l)
	if [ "$established" -ne $num_streams ]; then
		let diff=num_streams-established
		printf "${warning}%s${reset}: Only %d out of %d streams connected\n" "${args[@]}" $established $num_streams
		fail=1
	elif [ "${status[0]}" -ne $num_streams ]; then
		printf "${warning}%s${reset}: Only %d out of %d streams connected\n" "${args[@]}" $num_streams $num_streams
		fail=1
	fi

	# Check if any connections were reset by peer
	conns_reset=$(grep -hE "Connection reset by peer" "${basename}_sender_${stream_type}.log" | wc -l)
	if [ "$conns_reset" -gt 0 ]; then
		printf "${warning}%s${reset}: %d out of %d connections was reset by peer\n" "${args[@]}" $conns_reset $num_streams
		fail=1
	fi

	# Check if any connections timed out
	conns_timedout=$(grep -hE "Connection timed out" "${basename}_sender_${stream_type}.log" | wc -l)
	if [ "$conns_timedout" -gt 0 ]; then
		printf "${warning}%s${reset}: %d out of %d connections timed out\n" "${args[@]}" $conns_timedout $num_streams
		fail=1
	fi

	# Check if threads completed successfully
	if [ "${status[3]}" -gt 0 ]; then
		printf "${warning}%s${reset}: %d out of %d streams had errors\n" "${args[@]}" ${status[3]} $num_streams
		fail=1
	fi

	if [ "${status[4]}" -gt 0 ]; then
		printf "${warning}%s${reset}: %d out of %d streams had errors\n" "${args[@]}" ${status[4]} $num_streams
		fail=1
	fi

	return $fail
}


# Extract a parameter from filename
# Usage: extract_parameter <filename> <argument position>
extract_parameter()
{
	prefix="\./fair-stable"
	filename=$1
	position=$2

	query="^$prefix"
	while (( $position > 0 )); do
		query="$query-[^-]*"
		let position-=1
	done

	query="$query-\([^-]*\)"
	echo $(expr "$filename" : $query)
}


# Run tcp-throughput
# Usage: run_tcpthroughput <tracefile> <filename> <receiver start port> <number of streams>
run_tcpthroughput()
{
	trace=$1
	start_port=$3
	stream_count=$4
	filename=goodput/${2}

	let end_port=start_port+stream_count

	mkdir -p goodput
	$tcpthroughput -s $stream_sender_ip -r $stream_receiver_ip -p $start_port-$end_port -f $trace -t $sample_sec 1> ${filename} 2> ${filename}.error

	echo $filename
}


# Run tput
# Usage: run_tput <tracefile> <filename> <receiver start port> <number of streams>
run_tput()
{
	trace=$1
	start_port=$3
	stream_count=$4
	filename=throughput/${2}

	let end_port=start_port+stream_count

	mkdir -p throughput
	$tput -s $stream_sender_ip -r $stream_receiver_ip -p $start_port -P $end_port -t $sample_sec $trace 1> $filename 2> ${filename}.error

	echo $filename
}


# Run tcptrace
# Usage: run_tcptrace <tracefile> <filename>
run_tcptrace()
{
	tracefile=$1
	filename=$2
	$tcptrace -xtraffic $tracefile 1> /dev/null 2>&1
	mv traffic_byport.dat ${filename}.tput
	mv traffic_stats.dat ${filename}.stats

	echo ${filename}.tput ${filename}.stats
}


# Run analyseTCP
# Usage: bla
run_analysetcp()
{
	tracefile=$1
	filename=$2
	start_port=$3
	num_streams=$4
	let end_port=start_port+num_streams

	mkdir -p latency
	data=$($analysetcp -s $stream_sender_ip -r $stream_receiver_ip -p ${start_port}-${end_port} -f $tracefile -u${filename}_ -o latency -A)
	loss=$(echo $data | grep -Eoh "Estimated loss rate based on retransmissions\s*:\s*([0-9\.]+)\s*%" | grep -Eoh "([0-9\.]+)")

	echo latency/${filename}_all-aggr.dat latency/${filename}_all-duration.dat $loss
}


process_trace()
{
	tracefile=$1


	# global test parameters
	tcp_algorithm=$(extract_parameter $tracefile 0)
	test_qdisc=$(extract_parameter $tracefile 1)
	test_duration=$(expr "$(extract_parameter $tracefile 2)" : '\([0-9]*\)s')
	test_bandwidth=1000	# The bandwidth of the test
	test_roundtrip=150	# The RTT of the test

	# TCP options
	tcp_nagle=0 		# Nagle's algorithm is off
	tcp_collapse=0 		# TCP congestion collapse is off
	tcp_windowscaling=1 	# TCP window scaling is on

	# greedy stream parameters
	stream_greedy_count=$(expr "$(extract_parameter $tracefile 3)" : '\([0-9]*\)vs')

	# thin stream parameters
	stream_thin_count=$(expr "$(extract_parameter $tracefile 3)" : '[0-9]*vs\([0-9]*\)')
	stream_thin_intertransmission_time=$(expr "$(extract_parameter $tracefile 5)" : 'iat\([0-9]*\)')
	stream_thin_packet_size=$(expr "$(extract_parameter $tracefile 6)" : 'ps\([0-9]*\)')

	# TODO: Extract thin stream modifications
	stream_thin_retrans_mod=''

	# Write test configuration to file
	echo >> $config

	echo "trace" "$tracefile" >> $config

	echo "test_qdisc" "$test_qdisc" >> $config
	echo "test_duration" "$test_duration" >> $config
	echo "test_bandwidth" "$test_bandwidth" >> $config
	echo "test_roundtrip" "$test_roundtrip" >> $config

	echo "tcp_algorithm" "$tcp_algorithm" >> $config
	echo "tcp_nagle" "$tcp_nagle" >> $config
	echo "tcp_collapse" "$tcp_collapse" >> $config
	echo "tcp_windowscaling" "$tcp_windowscaling" >> $config
	echo "tcp_modification" "$stream_thin_retrans_mod" >> $config

	echo "stream_thin_remote_port" "$stream_thin_remote_port" >> $config
	echo "stream_thin_local_port" "$stream_thin_local_port" >> $config
	echo "stream_thin_count" "$stream_thin_count" >> $config
	echo "stream_thin_intertransmission_time" "$stream_thin_intertransmission_time" >> $config
	echo "stream_thin_packet_size" "$stream_thin_packet_size" >> $config
	echo "stream_greedy_count" "$stream_greedy_count" >> $config
	echo "stream_greedy_remote_port" "$stream_greedy_remote_port" >> $config
	echo "stream_greedy_local_port" "$stream_greedy_local_port" >> $config
	echo "stream_sender_ip" "$stream_sender_ip" >> $config
	echo "stream_receiver_ip" "$stream_receiver_ip" >> $config

	# Check logs for potential errors
	let total_count=stream_thin_count+stream_greedy_count
	check_trace_log $tracefile $total_count 
	if [ "$?" -gt 1 ] && [ "$force" -eq 0 ]; then
		echo $tracefile >> $error
		return
	fi

	check_streamzero_log $tracefile $stream_thin_remote_port 
	if [ "$?" -gt 1 ] && [ "$force" -eq 0 ]; then
		echo $tracefile >> $error
		return
	fi

	check_streamzero_log $tracefile $stream_greedy_remote_port 
	if [ "$?" -gt 1 ] && [ "$force" -eq 0 ]; then
		echo $tracefile >> $error
		return
	fi

	#let thin_port_end=stream_thin_remote_port+stream_thin_count
	#let greedy_port_end=stream_greedy_remote_port+stream_greedy_count

	# Analyze trace with tcptrace
	printf "${info}%s${reset}: Running tcptrace...\n" $tracefile
	files=($(run_tcptrace $tracefile "summary_${tcp_algorithm}_${test_qdisc}_${stream_thin_retrans_mod}_${stream_thin_count}"))
	echo "summary_stats" "${files[1]}" >> $config
	echo "summary_byport" "${files[0]}" >> $config

	# Analyze trace with tcp-throughput
	echo "goodput_sample_aggr_sec" "$sample_sec" >> $config
	printf "${info}%s${reset}: Running tcp-throughput...\n" $tracefile
	csv=$(run_tcpthroughput $tracefile "gput_thin_${tcp_algorithm}_${test_qdisc}_${stream_thin_retrans_mod}_${stream_thin_count}" $stream_thin_remote_port $stream_thin_count)
	echo "goodput_thin" "$csv" >> $config
	csv=$(run_tcpthroughput $tracefile "gput_greedy_${tcp_algorithm}_${test_qdisc}_${stream_thin_retrans_mod}_${stream_greedy_count}" $stream_greedy_remote_port $stream_greedy_count)
	echo "goodput_greedy" "$csv" >> $config

	# Analyze trace with tput
	echo "throughput_sample_aggr_sec" "$sample_sec" >> $config
	printf "${info}%s${reset}: Running tput...\n" $tracefile
	csv=$(run_tput $tracefile "tput_thin_${tcp_algorithm}_${test_qdisc}_${stream_thin_retrans_mod}_${stream_thin_count}" $stream_thin_remote_port $stream_thin_count)
	echo "throughput_thin" "$csv" >> $config
	csv=$(run_tput $tracefile "tput_greedy_${tcp_algorithm}_${test_qdisc}_${stream_thin_retrans_mod}_${stream_greedy_count}" $stream_greedy_remote_port $stream_greedy_count)
	echo "throughput_greedy" "$csv" >> $config

	# Analyze trace with analyseTCP
	printf "${info}%s${reset}: Running analyseTCP...\n" $tracefile
	data=($(run_analysetcp $tracefile "lat_thin_${tcp_algorithm}_${test_qdisc}_${streams_thin_retrans_mod}_${stream_thin_count}" $stream_thin_remote_port $stream_thin_count))
	echo "latency_aggr_thin" "${data[0]}" >> $config
	echo "duration_thin" "${data[1]}" >> $config
	echo "loss_thin" "${data[2]}" >> $config

	data=($(run_analysetcp $tracefile "lat_greedy_${tcp_algorithm}_${test_qdisc}_${streams_thin_retrans_mod}_${stream_greedy_count}" $stream_greedy_remote_port $stream_greedy_count))
	echo "latency_aggr_greedy" "${data[0]}" >> $config
	echo "duration_greedy" "${data[1]}" >> $config
	echo "loss_greedy" "${data[2]}" >> $config
}





echo -n > $config
echo -n > $error

# Find all packet traces and process them
files=$(find . -regextype posix-extended -regex "^\./fair-stable-[a-z]+-[a-z]+-[0-9]+s-[0-9]+vs[0-9]+-streams-iat[0-9]+-ps[0-9]+-(|-a|-m|-am)\.dump")

for file in $files; do
	process_trace $file
done
