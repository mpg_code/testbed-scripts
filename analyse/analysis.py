#!/usr/bin/env python

import re
import os
import sys

tcpthroughput='/home/jonas/master/tstools_tcp-throughput/tcp-throughput'
tput='/home/jonas/master/tput/tput'
analysetcp='/home/jonas/master/tstools_analysetcp/build/analyseTCP'
sample_sec=1


def create_filename(config, suffix, extension):
	name = 'fair-stable-%(bandwidth)dkbit-rtt%(roundtrip)d-%(tcp_algorithm)s-%(aqm_algorithm)s-%(duration)ds-%(num_thin)dvs%(num_greedy)d-streams-itt%(intertransmission_time)d-ps%(packet_size)d-' % config
	if not config['tcp_mechanism'] is None:
		name += config['tcp_mechanism']

	name += '_' + suffix
	name += '.' + extension
	return name



path = '.'
if len(sys.argv) >= 2:
	path = sys.argv[1]


# Collect sender-side dump files
sender_dumps = []

for filename in os.listdir(path):
	if os.path.isfile(os.path.join(path, filename)) and filename.endswith("_front.dump"):
		sender_dumps.append(filename)

if len(sender_dumps) == 0:
	print "No trace dumps found in %s" % path
	sys.exit(1)


expr = re.compile(r'fair-stable-(\d+)kbit-rtt(\d+)-([^-]+)-([^-]+)-(\d+)s-(\d+)vs(\d+)-streams-itt(\d+)-ps(\d+)-(-a|-am|-m)?_(front|after)\.dump')

# Extract information from filename
tests = []
for sender_dump in sender_dumps:
	match = re.match(expr, sender_dump)

	if match is None:
		print "Sender dump %s does not match recognized pattern" % sender_dump
		sys.exit(2)

	bandwidth, rtt, algorithm, queue, duration, thin_num, greedy_num, itt, packet_size, mechanism, side = match.groups()

	test_config = dict(
			# General test parameters
			bandwidth=int(bandwidth),
			roundtrip=int(rtt),
			aqm_algorithm=queue,
			duration=int(duration),
			sample_sec=sample_sec,

			# greedy stream options
			num_greedy=int(greedy_num),
			port_sender_greedy=15000,
			port_receiver_greedy=12345,
			port_receiver_greedy_last=12345 + int(greedy_num),
			host_sender_greedy='10.0.0.10',
			host_receiver_greedy='10.0.1.10',

			# thin stream options
			num_thin=int(thin_num),
			port_sender_thin=20000,
			port_receiver_thin=23456,
			port_receiver_thin_last=23456 + int(thin_num),
			host_sender_thin='10.0.0.10',
			host_receiver_thin='10.0.1.10',
			intertransmission_time=int(itt),
			packet_size=int(packet_size),

			# TCP options
			tcp_algorithm=algorithm,
			tcp_mechanism=mechanism,
			tcp_window_scaling=1,
			tcp_retrans_collapse=1,
			tcp_nagle=1,
			tcp_delayed_ack=0
			)

	c = lambda t, e: create_filename(test_config, t, e) if os.path.exists(os.path.join(path, create_filename(test_config, t, e))) else None

	test_data = {
		'sender_trace_dump': sender_dump, 'sender_trace_log': c('front', 'log'),
		'receiver_trace_dump': c('after', 'dump'), 'receiver_trace_log': c('after', 'log'),
		'sender_thin_log': c('sender_thin', 'log'), 'sender_greedy_log': c('sender_greedy', 'log'),
		'receiver_thin_log': c('receiver_thin','log'), 'receiver_greedy_log': c('receiver_greedy','log')
		}

	tests.append((test_config, test_data, {}))



# Parse and check logs for errors
for cfg, data, result in tests:

	# Check packet traces
	for logfile in 'sender_trace_log', 'receiver_trace_log':
		if data[logfile] is None:
			continue

		with open(data[logfile]) as f:
			log_data = f.read()
			match = re.search(r'(\d+) packets received by filter', log_data)
			if match is None:
				print "Malformed file: %s" % data[logfile]
				sys.exit(2)

			if int(match.group(1)) < 1000:
				print "Very few packets captured: %s" % data[logfile]
				sys.exit(3)

			match = re.search(r'(\d+) packets dropped', log_data)
			if match is None:
				print "Malformed file: %s" % data[logfile]
				sys.exit(2)

			if int(match.group(1)) > 0:
				print "Trace %s has %d dropped packets" % (data[logfile], int(match.group(1)))
				sys.exit(3)

	# Check streamzero logs
	for logfile in 'sender_thin_log', 'sender_greedy_log':
		if data[logfile] is None:
			continue

		with open(data[logfile]) as f:
			log_data = f.read()
			match = re.search(r'(\d+) threads out of (\d+) targeted arrived: (\d+) completed successfully, (\d+) errors and (\d+) aborted', log_data)
			if match is None:
				print "Incomplete log file: %s" % data[logfile]
				sys.exit(2)

			if match.group(1) != match.group(2) != match.group(3):
				print "Something is wrong, not all threads were successful: %s" % data[logfile]
				sys.exit(3)

			if int(match.group(4)) > 0:
				print "Something is wrong, threads with errors: %s" % data[logfile]
				sys.exit(3)

			match = re.search(r'No route to host', log_data)
			if not match is None:
				print "Something is wrong, couldn't connect: %s" % data[logfile]
				sys.exit(3)

			match = re.search(r'Connection reset by peer', log_data)
			if not match is None:
				print "Something is wrong, connections were reset: %s" % data[logfile]
				sys.exit(3)

			match = re.search(r'Connection timed out', log_data)
			if not match is None:
				print "Something is wrong, connections timed out: %s" % data[logfile]
				sys.exit(3)


# Run tcp-throughput
print "Running tcp-throughput..."
os.system('mkdir -p %s' % os.path.join(path, 'goodput'))

for config, data, results in tests:

	print "Processing %s" % data['sender_trace_dump']
	c = lambda t: os.path.join(path, 'goodput', create_filename(config, t, 'csv'))

	os.system(tcpthroughput + ' -s %(host_sender_thin)s -r %(host_receiver_thin)s -p %(port_receiver_thin)d-%(port_receiver_thin_last)d ' % config + ' -f %(trace)s -t %(sample_sec)d 1> %(filename)s 2> %(filename)s.error' % {'trace': data['sender_trace_dump'], 'sample_sec': sample_sec * 1000, 'filename': c('thin')})
	results['goodput_thin'] = c('thin')

	os.system(tcpthroughput + ' -s %(host_sender_greedy)s -r %(host_receiver_greedy)s -p %(port_receiver_greedy)d-%(port_receiver_greedy_last)d ' % config + ' -f %(trace)s -t %(sample_sec)d 1> %(filename)s 2> %(filename)s.error' % {'trace': data['sender_trace_dump'], 'sample_sec': sample_sec * 1000, 'filename': c('greedy')})
	results['goodput_greedy'] = c('greedy')




# Write test configurations to file
with open(os.path.join(path, 'config.txt'), 'w') as f:
	for config, data, results in tests:
		f.write(str(repr({'config': config, 'results': results})))
		f.write('\n')
