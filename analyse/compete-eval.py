#!/usr/bin/env python
import csv
import re
import numpy as np

def jain(X):
    if len(X) < 1: return 1.0
    if sum(X) == 0: return 1.0
    return float((sum(X)**2)) / (len(X)*sum([x**2 for x in X]))

mean = lambda l: float(sum(l))/len(l) if len(l) > 0 else float('nan')


def get_latencies(vs, tcp_variant, stream_type):
    ## latency statistics
    fn = "%s-%s--%s-all-aggr.dat" % (vs, tcp_variant, stream_type)

    latencies = []
    with open(fn) as f:
        for l in f:
        	latencies.append(float(l))
    return latencies

def get_losses(vs, tcp_variant, stream_type):
    ## loss rate
    losses = []
    fn = "%s-%s--%s.log" % (vs, tcp_variant, stream_type)
    with open(fn) as f:

        for l in f:
        	if not l.startswith("Estimated loss"): continue
        	m = re.match(".*: *(\d+\.\d+) %.*", l)
        	if m is not None:
        		losses.append(float(m.groups()[0]))
    if len(losses) == 0:
        print fn
    return losses

def get_throughput(vs, tcp_variant, stream_type):
    # XXX remove me ;)
    #return [1]
    # n-reno-pfifo-1-12345.csv
    if tcp_variant != "": return [1]
    num = vs.split("vs")[0]
    port = "12345" if stream_type == "greedy" else "23456"
    fn = "n-reno-%s-%s-%s.csv" % (tcp_variant, num, port)
    throughput = []
    with open(fn, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        first = True
        for row in reader:
            if not first:
                throughput.extend([ (8 * float(x)/(30.0 * 1000)) for x in row ])
            else:
                first = False
    return throughput

#tcp_variants = [ "a.dump", "am.dump", "m.dump", ""]
#tcp_variants = [ "" ]
tcp_variants = [ "pfifo", "bfifo", "sfq", "sfb", "codel" ]
competitors =  [ "1vs1", "2vs2", "4vs4", "8vs8", "16vs16", "32vs32", "64vs64", "128vs128", "256vs256", "512vs512"]

losses = {}
latencies = {}
throughput = {}
total_latencies = {}
mean_latencyg = {}
mean_latencyt = {}
max_latencyg = {}
max_latencyt = {}
percentile_latencyg = {}
percentile_latencyt = {}
jfi_latency = {}
total_losses = {}
mean_lossg = {}
mean_losst = {}
max_lossg = {}
max_losst = {}
percentile_lossg = {}
percentile_losst = {}
jfi_loss = {}
total_throughput = {}
mean_throughputg = {}
mean_throughputt = {}
max_throughputg = {}
max_throughputt = {}
percentile_throughputg = {}
percentile_throughputt = {}
jfi_throughput = {}
for vs in competitors:
    losses[vs] = {}
    latencies[vs] = {}
    throughput[vs] = {}
    total_latencies[vs] = {}
    mean_latencyg[vs] = {}
    mean_latencyt[vs] = {}
    max_latencyg[vs] = {}
    max_latencyt[vs] = {}
    percentile_latencyg[vs] = {}
    percentile_latencyt[vs] = {}
    jfi_latency[vs] = {}
    total_losses[vs] = {}
    mean_lossg[vs] = {}
    mean_losst[vs] = {}
    max_lossg[vs] = {}
    max_losst[vs] = {}
    percentile_lossg[vs] = {}
    percentile_losst[vs] = {}
    jfi_loss[vs] = {}
    total_throughput[vs] = {}
    mean_throughputg[vs] = {}
    mean_throughputt[vs] = {}
    max_throughputg[vs] = {}
    max_throughputt[vs] = {}
    percentile_throughputg[vs] = {}
    percentile_throughputt[vs] = {}
    jfi_throughput[vs] = {}
    for v in tcp_variants:
        losses[vs][v] = {}
        latencies[vs][v] = {}
        throughput[vs][v] = {}
        total_latencies[vs][v] = {}
        mean_latencyg[vs][v] = {}
        mean_latencyt[vs][v] = {}
        max_latencyg[vs][v] = {}
        max_latencyt[vs][v] = {}
        percentile_latencyg[vs][v] = {}
        percentile_latencyt[vs][v] = {}
        total_losses[vs][v] = {}
        mean_lossg[vs][v] = {}
        mean_losst[vs][v] = {}
        max_lossg[vs][v] = {}
        max_losst[vs][v] = {}
        percentile_lossg[vs][v] = {}
        percentile_losst[vs][v] = {}
        total_throughput[vs][v] = {}
        mean_throughputg[vs][v] = {}
        mean_throughputt[vs][v] = {}
        max_throughputg[vs][v] = {}
        max_throughputt[vs][v] = {}
        percentile_throughputg[vs][v] = {}
        percentile_throughputt[vs][v] = {}


for vs in competitors:
    for v in tcp_variants:
        latencies[vs][v]["greedy"] = get_latencies(vs, v, "greedy")
        latencies[vs][v]["thin"] = get_latencies(vs, v, "thin")
        losses[vs][v]["greedy"] = get_losses(vs, v, "greedy")
        losses[vs][v]["thin"] = get_losses(vs, v, "thin")
        throughput[vs][v]["greedy"] = get_throughput(vs, v, "greedy")
        throughput[vs][v]["thin"] = get_throughput(vs, v, "thin")

    for v in tcp_variants:
        total_latencies[vs][v] = []
        total_latencies[vs][v].extend(latencies[vs][v]["thin"])
        total_latencies[vs][v].extend(latencies[vs][v]["greedy"])
        jfi_latency[vs][v] = jain(total_latencies[vs][v])
        total_losses[vs][v] = []
        total_losses[vs][v].extend(losses[vs][v]["thin"])
        total_losses[vs][v].extend(losses[vs][v]["greedy"])
        jfi_loss[vs][v] = jain(total_losses[vs][v])
        total_throughput[vs][v] = []
        total_throughput[vs][v].extend(throughput[vs][v]["thin"])
        total_throughput[vs][v].extend(throughput[vs][v]["greedy"])
        jfi_throughput[vs][v] = jain(total_throughput[vs][v])

    for v in tcp_variants:
        mean_latencyg[vs][v] = mean(latencies[vs][v]["greedy"])
        mean_latencyt[vs][v] = mean(latencies[vs][v]["thin"])
        mean_lossg[vs][v] = mean(losses[vs][v]["greedy"])
        mean_losst[vs][v] = mean(losses[vs][v]["thin"])
        mean_throughputg[vs][v] = mean(throughput[vs][v]["greedy"])
        mean_throughputt[vs][v] = mean(throughput[vs][v]["thin"])

        max_latencyg[vs][v] = max(latencies[vs][v]["greedy"])
        max_latencyt[vs][v] = max(latencies[vs][v]["thin"])
        max_lossg[vs][v] = max(losses[vs][v]["greedy"])
        max_losst[vs][v] = max(losses[vs][v]["thin"])
        max_throughputg[vs][v] = max(throughput[vs][v]["greedy"])
        max_throughputt[vs][v] = max(throughput[vs][v]["thin"])

        percentile_latencyg[vs][v] = np.percentile(latencies[vs][v]["greedy"], 75)
        percentile_latencyt[vs][v] = np.percentile(latencies[vs][v]["thin"], 75)
        percentile_lossg[vs][v] = np.percentile(losses[vs][v]["greedy"], 75)
        percentile_losst[vs][v] = np.percentile(losses[vs][v]["thin"], 75)
        percentile_throughputg[vs][v] = np.percentile(throughput[vs][v]["greedy"], 75)
        percentile_throughputt[vs][v] = np.percentile(throughput[vs][v]["thin"], 75)

with open("tables.tex", "w") as fw:
    for v in tcp_variants:
        ##############
        ## Latency table
        fw.write("""
\\begin{table}[h!]
\\caption{Latency in ms for an equal number of competing thin and greedy streams (%s).}

\\begin{tabular}{|l|l|l|l|l|l|l||l|} \\hline
\\multirow{2}{*}{Competitors}  %%\\hline
& \\multicolumn{2}{|c|}{mean} & \\multicolumn{2}{|c|}{max} & \\multicolumn{2}{|c||}{Q3} & \multirow{2}{*}{$JFI_{lat}$} \\\\
& greedy & thin & greedy & thin & greedy & thin & \\\\ \\hline
""" % v)

        for vs in competitors:
            fw.write("%s\t & %.0f\t & %.0f\t & %.0f\t & %.0f\t & %.0f\t & %.0f\t & %.2f \\\\ \hline" % (vs,
                    mean_latencyg[vs][v], mean_latencyt[vs][v],
                    max_latencyg[vs][v], max_latencyt[vs][v],
                    percentile_latencyg[vs][v], percentile_latencyt[vs][v], jfi_latency[vs][v]))

        fw.write("\\end{tabular} \\end{table}")
        fw.write("\n")

        ## losses table
        fw.write("""
\\begin{table}[h!]
\\caption{Loss rate in percent for an equal number of competing thin and greedy streams (%s).}

\\begin{tabular}{|l|l|l|l|l|l|l||l|} \\hline
\\multirow{2}{*}{Competitors}  %%\\hline
& \\multicolumn{2}{|c|}{mean} & \\multicolumn{2}{|c|}{max} & \\multicolumn{2}{|c||}{Q3} & \multirow{2}{*}{$JFI_{loss}$} \\\\
& greedy & thin & greedy & thin & greedy & thin & \\\\ \\hline
""" % v)

        for vs in competitors:
            fw.write("%s\t & %.0f\t & %.0f\t & %.0f\t & %.0f\t & %.0f\t & %.0f\t & %.2f \\\\ \hline" % (vs,
                    mean_lossg[vs][v], mean_losst[vs][v],
                    max_lossg[vs][v], max_losst[vs][v],
                    percentile_lossg[vs][v], percentile_losst[vs][v], jfi_loss[vs][v]))

        fw.write("\\end{tabular} \\end{table}")
        fw.write("\n")

        ## throughput table
#        fw.write("""
#\\begin{table}[h]
#\\caption{throughput for an equal number of competing thin and greedy streams (%s).}
#
#\\begin{tabular}{|l|l|l|l|l|l|l||l|} \\hline
#\\multirow{2}{*}{Competitors}  %%\\hline
#& \\multicolumn{2}{|c|}{mean} & \\multicolumn{2}{|c|}{max} & \\multicolumn{2}{|c||}{Q3} & \multirow{2}{*}{$JFI_{tp}$} \\\\
#& greedy & thin & greedy & thin & greedy & thin & \\\\ \\hline
#""" % v)
#
#        for vs in competitors:
#            fw.write("%s\t & %.0f\t & %.0f\t & %.0f\t & %.0f\t & %.0f\t & %.0f\t & %.2f \\\\ \hline" % (vs,
#                    mean_throughputg[vs][v], mean_throughputt[vs][v],
#                    max_throughputg[vs][v], max_throughputt[vs][v],
#                    percentile_throughputg[vs][v], percentile_throughputt[vs][v], jfi_throughput[vs][v]))
#
#        fw.write( "\\end{tabular} \\end{table}" )
#        fw.write("\n")
