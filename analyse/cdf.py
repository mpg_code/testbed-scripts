#!/usr/bin/env python
import matplotlib
matplotlib.use("Agg")
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
from sys import stdout

nums = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512]
queues = ['pfifo', 'bfifo', 'sfq', 'sfb', 'codel']
limit = 10000

def output(s):
	stdout.write(s)
	stdout.flush()


def read_latencies(num_streams, queue, stream_type):
	filename = "%dvs%d-%s--%s-all-aggr.dat" % (num_streams, num_streams, queue, stream_type)
	output( 'processing %s...' % filename )

	with open(filename) as f:
		latencies = [np.float32(l) for l in f]

	output('done\n')
	return np.array(latencies, dtype=np.float32)


def plot_hist(ax, latencies):
	output('plotting histogram...')
	lat = np.sort(latencies)
	plt.plot(lat, np.arange(len(lat) * 1.0) / len(lat))
	output('done\n')
	return max(lat)



pp = PdfPages('cdf.pdf')
for num in nums:
	for queue in queues:
		fig, ((ax)) = plt.subplots(nrows=1, ncols=1)

		ax.set_title('%dvs%d %s' % (num, num, queue))

		ax.set_ylim(0, 1.1)

		thin_max = plot_hist(ax, read_latencies(num, queue, 'thin'))
		greedy_max = plot_hist(ax, read_latencies(num, queue, 'greedy'))

		if limit > 0:
			ax.set_xlim(0, min(greedy_max, thin_max, limit))

		plt.tight_layout()
		plt.savefig(pp, format='pdf')
pp.close()
