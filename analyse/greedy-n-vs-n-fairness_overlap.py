#!/usr/bin/env python
import os
import sys
from rpy2.robjects import r

# To be run in the  same folder as the dump files.
# Need to specify test duration (in seconds) and recv port
# in the test parameters

### Test parameters

numStreams = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512]
# numGreedy = numStreams
numGreedy = [5] * len(numStreams)
mechanisms = { 'TCP': "", 'mFR': "-a", 'LT': "-m", 'LT+mFR': "-am" }
order = ['TCP', 'mFR', 'LT', 'LT+mFR']
#qdiscs = [ 'bfifo', 'pfifo' ]
qdiscs = [ 'pfifo' ]
#algorithms = [ 'reno', 'cubic' ]
algorithms = [ 'reno' ]
dstPorts = { 12345: 'greedy', 23456: 'thin' }
srcPorts = { 12345: 15000, 23456: 20000 }
duration = 7200
iat = 150
packetSize = 120

sampleSec = 30

### End test parameters

outFile = "test.ps"
pcapSuffix = ".dump"
analysePrefix = 'analyse'
bandwidth = 1000
revMap = dict([ (val, key) for key, val in mechanisms.items() ])

tests = []
for queue in qdiscs:
	for algorithm in algorithms:
		for port in dstPorts.keys():
			plot_set = []

			for idx, num in enumerate(numStreams):
				files = []

				for mech in [ mechanisms[key] for key in sorted(mechanisms, key=lambda x: order.index(x))]:
					csv = "n-%s-%s%s-%d-%d.csv" % (algorithm, queue, mech, num, port)
					pcap = "fair-stable-%s-%s-%ds-%dvs%d-streams-iat%d-ps%d-%s" % (algorithm, queue, duration, numGreedy[idx], num, iat, packetSize, mech)
					pcap += pcapSuffix
					files.append((numGreedy[idx], num, csv, pcap, algorithm, queue, mech, port))
				plot_set.append(files)

			tests.append(plot_set)

analysis = {}
psets = []
for plot_set in tests:
	plots = []
	for plot in plot_set:

		plot_vars = []

		for numGreedy, num, csv, pcap, algorithm, queue, mech, port in plot:

			print "Processing " + pcap
			title = '%d greedy vs %d thin' % (numGreedy, num)

			if not os.path.isfile(str(csv)):
				err = csv.split('.')[0] + '.err'
				os.system("./tcp-throughput -s 10.0.0.10 -r 10.0.1.10 -p " + str(port) + " -f " + pcap + " -t " + str(int(sampleSec * 1000)) + " 1> " + str(csv) + " 2> " + str(err))

			#analysisFiles = ['%s-%s-all-10.0.0.10-%d-10.0.1.10-%d' % (analysePrefix, pcap, (srcPorts[port]+i), port) for i in xrange(0, numGreedy if dstPorts[port] == 'gredy' else num)]

			v = str(port) + csv.split('.')[0].replace('-', '_')
			l = revMap[mech]
			plot_vars.append((v, l, num, numGreedy))

			r("v_%s <- ((colSums(read.csv(file=\"%s\", header=TRUE, sep=\",\"))) / %d) * 8" % (v, csv, sampleSec))
			r("v_%s <- v_%s[2:length(v_%s) - 1]" % (v, v, v))

		plots.append((title, plot_vars))
	psets.append(plots)




def xline(bw, thin_ps, thin_iat, thin_num, greedy_num):
	# how much the thin streams would use if alone
	x = int(thin_num * (((thin_ps+40+36)*8) / float(thin_iat/1000.0)))
	y = ((bw*1000.0)/float(thin_num+greedy_num))*thin_num

	print thin_num, x, y, min(x, y)
	return min(x, y)


r("postscript(\"" + outFile + "\")")
#for plot_set in psets:
if 1:

	#r("plot.new()")
	r("offset <- 1.85")
	r("layout(matrix(c(1,2,3,4,5, 6,7,8,9,10), 2, 5, byrow=T), widths=c(5.5,4,4,4,4), heights=c(5.9,8))")

	index = 0
	#for idx, plotdata in enumerate(plot_set):
	for idx, plotdata in enumerate(psets[0]):
		title = plotdata[0]
		plot = plotdata[1]

		num = plot[0][2]
		numGreedy = plot[0][3]

		if index == 0:
			r("par(mar=c(1,5,2,0), xaxt=\"n\", cex.axis=1.15)")
		elif index == len(plot_set) // 2:
			r("par(mar=c(10,5,2,0), xaxt=\"n\", yaxt=\"s\")")
		elif index == 1 or index == len(plot_set) // 2 + 1:
			r("par(mar=c(%d,0,2,0), yaxt=\"n\")" % (1 if index < len(plot_set) // 2 else 10))

		r("boxplot(%s, border=\"darkblue\", main=\"%s\", ylim=c(0,%d), xaxt=\"n\", xlab=\"\", yaxt=\"n\")" % (", ".join(["v_%s" % p[0] for p in plot]), title, (bandwidth * 1000)))
		r("boxplot(%s, border=\"darkred\", main=\"%s\", ylim=c(0,%d), xaxt=\"n\", xlab=\"\", yaxt=\"n\", add=TRUE)" % (", ".join(["v_%s" % p[0] for p in psets[1][idx][1]]), title, (bandwidth * 1000)))

		# n-vs-n
		#r("abline(h=%d, lty=2)" % (min(int((num * ((packetSize*8+40+36) / float(iat/1000.0)))), (bandwidth*1000.0) / 2 )))

		# m-vs-n
		r("abline(h=%d, lty=2)" % xline(bandwidth, packetSize, iat, num, numGreedy))


		if index % (len(plot_set) // 2) == 0:
			r("axis(2, at=c(%s), labels=c(%s))" % (", ".join(["%d" % (i * 1000) for i in xrange(0, bandwidth+100, 100)]), ", ".join(['"%d"' % i for i in xrange(0, bandwidth+100, 100)])))

		if index >= (len(plot_set) // 2):
			r("text(1:6, par(\"usr\")[3] * offset, srt = 60, adj = 1, labels=c(%s), xpd=TRUE, cex=1.15)" % (",".join(['"%s"'%p[1] for p in plot])))
			r("axis(1, at=c(%s), labels=c(%s))" % (", ".join(["%d" % i for i in xrange(1, len(plot)+1)]), ", ".join(['"%s"' % p[1] for p in plot])))
		index += 1

	r("mtext(\"Throughput (Kbit/second aggregated over " + str(sampleSec) + " seconds)\", side = 2, line = -1.2, outer=TRUE, cex=0.95)")
	r("mtext(\"TCP variation used for competing streams\", side = 1, line = -3, outer=TRUE, cex=0.95)")

r("dev.off()")

