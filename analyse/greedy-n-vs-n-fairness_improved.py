#!/usr/bin/env python
import os
import sys
from rpy2.robjects import r

# To be run in the  same folder as the dump files.
# Need to specify test duration (in seconds) and recv port
# in the test parameters

### Test parameters

numStreams = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512]
mechanisms = { 'TCP': "", 'mFR': "-a", 'LT': "-m", 'LT+mFR': "-am" }
order = ['TCP', 'mFR', 'LT', 'LT+mFR']
qdiscs = [ 'bfifo', 'pfifo' ]
algorithms = [ 'reno', 'cubic' ]
dstPorts = { 'greedy': 12345, 'thin': 23456 }
duration = 600
iat = 150
packetSize = 120

sampleSec = 30

### End test parameters

outFile = "test.ps"
#pcapSuffix = "_172.16.0.31_br0.pcap"
pcapSuffix = ".dump"
bandwidth = 1000



tests = []
for num in numStreams:
	for queue in qdiscs:
		for algorithm in algorithms:
			for flowtype, port in dstPorts.items():
				files = []
				title = "%d greedy vs %d thin using %s (%s, %s)" % (num, num, queue, algorithm, flowtype)

				for mech in [ mechanisms[key] for key in sorted(mechanisms, key=lambda x: order.index(x))]:
					csv = "n-%s-%s%s-%d-%d.csv" % (algorithm, queue, mech, num, port)
					pcap = "fair-stable-%s-%s-%ds-%dvs%d-streams-iat%d-ps%d-%s" % (algorithm, queue, duration, num, num, iat, packetSize, mech)
					pcap += pcapSuffix
					files.append((csv, pcap, algorithm, queue, mech, port))

				tests.append((title, files))



var = []
revMap = dict([ (val, key) for key, val in mechanisms.items() ])
for title, files in tests:
	plot = []
	for csv, pcap, algorithm, queue, mech, port in files:

		print "Processing " + pcap

		if not os.path.isfile(str(csv)):
			err = csv.split('.')[0] + '.err'
			os.system("./tcp-throughput -s 10.0.0.10 -r 10.0.1.10 -p " + str(port) + " -f " + pcap + " -t " + str(int(sampleSec * 1000)) + " 1> " + str(csv) + " 2> " + str(err))

		v = str(port) + csv.split('.')[0].replace('-', '_')
		l = revMap[mech]
		plot.append((v, l))

		r("v_%s <- ((colSums(read.csv(file=\"%s\", header=TRUE, sep=\",\"))) / %d) * 8" % (v, csv, sampleSec))
		r("v_%s <- v_%s[2:length(v_%s) - 1]" % (v, v, v))

	var.append((title, plot))




r("postscript(\"" + outFile + "\")")
for title, plots in var:
	r("boxplot(%s, main=\"%s\", ylim=c(0,%d), xaxt=\"n\", xlab=\"\", yaxt=\"n\")" % (", ".join(["v_%s" % plot[0] for plot in plots]), title, (bandwidth * 1000)))
	r("mtext(\"Throughput (Kbit/second aggregated over " + str(sampleSec) + " seconds)\", side = 2, line = -1.2, outer=TRUE, cex=0.95)")

	r("axis(1, at=c(%s), labels=c(%s))" % (", ".join(["%d" % i for i in xrange(1, len(plots)+1)]), ", ".join(['"%s"' % plot[1] for plot in plots])))
	r("axis(2, at=c(%s), labels=c(%s))" % (", ".join(["%d" % (i * 1000) for i in xrange(0, bandwidth+100, 100)]), ", ".join(['"%d"' % i for i in xrange(0, bandwidth+100, 100)])))

r("dev.off()")
