#!/usr/bin/env python
import os
import sys
from rpy2.robjects import r

# To be run in the  same folder as the dump files.
# Need to specify test duration (in seconds) and recv port
# in the test parameters

### Test parameters

num_streams = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512]
dur = 30 # duration of test
#dur = 1800 # duration of test
stype = "thin" # Uncomment to analyse thin streams instead of greedy

### End test parameters

dstPort = 12345
outFile = "greedy-tp.fairness.ps"

if stype == "thin":
    dstPort = 23456
    outFile = "greedy-tp.fairness-thin.ps"

for n in num_streams:
#	fnt = "fair-stable-" + str(dur) + "s-" + str(n) + "-vs" + str(n) + "-streams-iat85-ps120--it1.pcap"
#	fnr = "fair-stable-" + str(dur) + "s-" + str(n) + "-vs" + str(n) + "-streams-iat85-ps120--r-it1.pcap"
#	fna = "fair-stable-" + str(dur) + "s-" + str(n) + "-vs" + str(n) + "-streams-iat85-ps120--a-it1.pcap"
#	fnm = "fair-stable-" + str(dur) + "s-" + str(n) + "-vs" + str(n) + "-streams-iat85-ps120--m-it1.pcap"
#	fnam = "fair-stable-" + str(dur) + "s-" + str(n) + "-vs" + str(n) + "-streams-iat85-ps120--am-it1.pcap"
	#fnram = "fair-stable-" + str(dur) + "s-" + str(n) + "-vs" + str(n) + "-streams-iat85-ps120--ram-it1.pcap"
	fnram = "fair-stable-" + str(dur) + "s-" + str(n) + "-vs" + str(n) + "-streams-iat85-ps120--ram_172.16.0.32_br0.pcap"


#	tf = "n" + str(n) + "-t.csv"
#	if not os.path.isfile(tf):
#		print "./tcp-throughput -s 10.0.0.10 -r 10. -p 23456 -f " + fnt + " -t 30000 > " + tf
#		os.system("./tcp-throughput -s 10.0.0.10 -r 10.0.1.10 -p " + str(dstPort) + " -f " + fnt + " -t 30000 > " + tf)
#	tf = "n" + str(n) + "-r.csv"
#	if not os.path.isfile(tf):
#		os.system("./tcp-throughput -s 10.0.0.10 -r 10.0.1.10 -p " + str(dstPort) + " -f " + fnr + " -t 30000 > " + tf)
#	tf = "n" + str(n) + "-a.csv"
#	if not os.path.isfile(tf):
#		os.system("./tcp-throughput -s 10.0.0.10 -r 10.0.1.10 -p " + str(dstPort) + " -f " + fna + " -t 30000 > " + tf)
#	tf = "n" + str(n) + "-m.csv"
#	if not os.path.isfile(tf):
#		os.system("./tcp-throughput -s 10.0.0.10 -r 10.0.1.10 -p " + str(dstPort) + " -f " + fnm + " -t 30000 > " + tf)
#	tf = "n" + str(n) + "-am.csv"
#	if not os.path.isfile(tf):
#		os.system("./tcp-throughput -s 10.0.0.10 -r 10.0.1.10 -p " + str(dstPort) + " -f " + fnam + " -t 30000 > " + tf)
	tf = "n" + str(n) + "-ram.csv"
	if not os.path.isfile(tf):
		os.system("./tcp-throughput -s 10.0.0.10 -r 10.0.1.10 -p " + str(dstPort) + " -f " + fnram + " -t 30000 > " + tf)


#r("n1stpt <- ((colSums(read.csv(file=\"n1-t.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n1stpr <- ((colSums(read.csv(file=\"n1-r.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n1stpa <- ((colSums(read.csv(file=\"n1-a.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n1stpm <- ((colSums(read.csv(file=\"n1-m.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n1stpam <- ((colSums(read.csv(file=\"n1-am.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
r("n1stpram <- ((colSums(read.csv(file=\"n1-ram.csv\", header=TRUE, sep=\",\"))) / 30) * 8")

#r("n1stpt <- n1stpt[2:length(n1stpt) - 1]")
#r("n1stpr <- n1stpr[2:length(n1stpr) - 1]")
#r("n1stpa <- n1stpa[2:length(n1stpa) - 1]")
#r("n1stpm <- n1stpm[2:length(n1stpm) - 1]")
#r("n1stpam <- n1stpam[2:length(n1stpam) - 1]")
r("n1stpram <- n1stpram[2:length(n1stpram) - 1]")

#r("n2stpt <- ((colSums(read.csv(file=\"n2-t.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n2stpr <- ((colSums(read.csv(file=\"n2-r.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n2stpa <- ((colSums(read.csv(file=\"n2-a.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n2stpm <- ((colSums(read.csv(file=\"n2-m.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n2stpam <- ((colSums(read.csv(file=\"n2-am.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
r("n2stpram <- ((colSums(read.csv(file=\"n2-ram.csv\", header=TRUE, sep=\",\"))) / 30) * 8")

#r("n2stpt <- n2stpt[2:length(n2stpt) - 1]")
#r("n2stpr <- n2stpr[2:length(n2stpr) - 1]")
#r("n2stpa <- n2stpa[2:length(n2stpa) - 1]")
#r("n2stpm <- n2stpm[2:length(n2stpm) - 1]")
#r("n2stpam <- n2stpam[2:length(n2stpam) - 1]")
r("n2stpram <- n2stpram[2:length(n2stpram) - 1]")

#r("n4stpt <- ((colSums(read.csv(file=\"n4-t.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n4stpr <- ((colSums(read.csv(file=\"n4-r.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n4stpa <- ((colSums(read.csv(file=\"n4-a.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n4stpm <- ((colSums(read.csv(file=\"n4-m.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n4stpam <- ((colSums(read.csv(file=\"n4-am.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n4stpram <- ((colSums(read.csv(file=\"n4-ram.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#
#r("n4stpt <- n4stpt[2:length(n4stpt) - 1]")
#r("n4stpr <- n4stpr[2:length(n4stpr) - 1]")
#r("n4stpa <- n4stpa[2:length(n4stpa) - 1]")
#r("n4stpm <- n4stpm[2:length(n4stpm) - 1]")
#r("n4stpam <- n4stpam[2:length(n4stpam) - 1]")
#r("n4stpram <- n4stpram[2:length(n4stpram) - 1]")
#
#r("n8stpt <- ((colSums(read.csv(file=\"n8-t.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n8stpr <- ((colSums(read.csv(file=\"n8-r.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n8stpa <- ((colSums(read.csv(file=\"n8-a.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n8stpm <- ((colSums(read.csv(file=\"n8-m.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n8stpam <- ((colSums(read.csv(file=\"n8-am.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n8stpram <- ((colSums(read.csv(file=\"n8-ram.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#
#r("n8stpt <- n8stpt[2:length(n8stpt) - 1]")
#r("n8stpr <- n8stpr[2:length(n8stpr) - 1]")
#r("n8stpa <- n8stpa[2:length(n8stpa) - 1]")
#r("n8stpm <- n8stpm[2:length(n8stpm) - 1]")
#r("n8stpam <- n8stpam[2:length(n8stpam) - 1]")
#r("n8stpram <- n8stpram[2:length(n8stpram) - 1]")
#
#r("n16stpt <- ((colSums(read.csv(file=\"n16-t.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n16stpr <- ((colSums(read.csv(file=\"n16-r.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n16stpa <- ((colSums(read.csv(file=\"n16-a.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n16stpm <- ((colSums(read.csv(file=\"n16-m.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n16stpam <- ((colSums(read.csv(file=\"n16-am.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n16stpram <- ((colSums(read.csv(file=\"n16-ram.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#
#r("n16stpt <- n16stpt[2:length(n16stpt) - 1]")
#r("n16stpr <- n16stpr[2:length(n16stpr) - 1]")
#r("n16stpa <- n16stpa[2:length(n16stpa) - 1]")
#r("n16stpm <- n16stpm[2:length(n16stpm) - 1]")
#r("n16stpam <- n16stpam[2:length(n16stpam) - 1]")
#r("n16stpram <- n16stpram[2:length(n16stpram) - 1]")
#
#r("n32stpt <- ((colSums(read.csv(file=\"n32-t.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n32stpr <- ((colSums(read.csv(file=\"n32-r.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n32stpa <- ((colSums(read.csv(file=\"n32-a.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n32stpm <- ((colSums(read.csv(file=\"n32-m.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n32stpam <- ((colSums(read.csv(file=\"n32-am.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n32stpram <- ((colSums(read.csv(file=\"n32-ram.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#
#r("n32stpt <- n32stpt[2:length(n32stpt) - 1]")
#r("n32stpr <- n32stpr[2:length(n32stpr) - 1]")
#r("n32stpa <- n32stpa[2:length(n32stpa) - 1]")
#r("n32stpm <- n32stpm[2:length(n32stpm) - 1]")
#r("n32stpam <- n32stpam[2:length(n32stpam) - 1]")
#r("n32stpram <- n32stpram[2:length(n32stpram) - 1]")
#
#r("n64stpt <- ((colSums(read.csv(file=\"n64-t.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n64stpr <- ((colSums(read.csv(file=\"n64-r.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n64stpa <- ((colSums(read.csv(file=\"n64-a.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n64stpm <- ((colSums(read.csv(file=\"n64-m.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n64stpam <- ((colSums(read.csv(file=\"n64-am.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n64stpram <- ((colSums(read.csv(file=\"n64-ram.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#
#r("n64stpt <- n64stpt[2:length(n64stpt) - 1]")
#r("n64stpr <- n64stpr[2:length(n64stpr) - 1]")
#r("n64stpa <- n64stpa[2:length(n64stpa) - 1]")
#r("n64stpm <- n64stpm[2:length(n64stpm) - 1]")
#r("n64stpam <- n64stpam[2:length(n64stpam) - 1]")
#r("n64stpram <- n64stpram[2:length(n64stpram) - 1]")
#
#r("n128stpt <- ((colSums(read.csv(file=\"n128-t.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n128stpr <- ((colSums(read.csv(file=\"n128-r.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n128stpa <- ((colSums(read.csv(file=\"n128-a.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n128stpm <- ((colSums(read.csv(file=\"n128-m.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n128stpam <- ((colSums(read.csv(file=\"n128-am.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n128stpram <- ((colSums(read.csv(file=\"n128-ram.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#
#r("n128stpt <- n128stpt[2:length(n128stpt) - 1]")
#r("n128stpr <- n128stpr[2:length(n128stpr) - 1]")
#r("n128stpa <- n128stpa[2:length(n128stpa) - 1]")
#r("n128stpm <- n128stpm[2:length(n128stpm) - 1]")
#r("n128stpam <- n128stpam[2:length(n128stpam) - 1]")
#r("n128stpram <- n128stpram[2:length(n128stpram) - 1]")
#
#r("n256stpt <- ((colSums(read.csv(file=\"n256-t.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n256stpr <- ((colSums(read.csv(file=\"n256-r.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n256stpa <- ((colSums(read.csv(file=\"n256-a.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n256stpm <- ((colSums(read.csv(file=\"n256-m.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n256stpam <- ((colSums(read.csv(file=\"n256-am.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n256stpram <- ((colSums(read.csv(file=\"n256-ram.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#
#r("n256stpt <- n256stpt[2:length(n256stpt) - 1]")
#r("n256stpr <- n256stpr[2:length(n256stpr) - 1]")
#r("n256stpa <- n256stpa[2:length(n256stpa) - 1]")
#r("n256stpm <- n256stpm[2:length(n256stpm) - 1]")
#r("n256stpam <- n256stpam[2:length(n256stpam) - 1]")
#r("n256stpram <- n256stpram[2:length(n256stpram) - 1]")
#
#r("n512stpt <- ((colSums(read.csv(file=\"n512-t.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n512stpr <- ((colSums(read.csv(file=\"n512-r.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n512stpa <- ((colSums(read.csv(file=\"n512-a.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n512stpm <- ((colSums(read.csv(file=\"n512-m.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n512stpam <- ((colSums(read.csv(file=\"n512-am.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#r("n512stpram <- ((colSums(read.csv(file=\"n512-ram.csv\", header=TRUE, sep=\",\"))) / 30) * 8")
#
#r("n512stpt <- n512stpt[2:length(n512stpt) - 1]")
#r("n512stpr <- n512stpr[2:length(n512stpr) - 1]")
#r("n512stpa <- n512stpa[2:length(n512stpa) - 1]")
#r("n512stpm <- n512stpm[2:length(n512stpm) - 1]")
#r("n512stpam <- n512stpam[2:length(n512stpam) - 1]")
#r("n512stpram <- n512stpram[2:length(n512stpram) - 1]")

#r("labels <- c(\"TCP\", \"mFR\", \"LT\", \"LT+mFR\",  \"RDB\", \"all\")")
r("postscript(\"" + outFile + "\")")
r("offset <- 1.85")
r("layout(matrix(c(1,2,3,4,5, 6,7,8,9, 10), 2, 5, byrow=T), widths=c(5.5,4,4,4,4), heights=c(5.9,8))")
r("par(mar=c(1,5,2,0), xaxt=\"n\", cex.axis=1.15)")
#r("boxplot(n1stpt, n1stpm, n1stpa, n1stpam, n1stpr, n1stpram, main=\"1 greedy vs 1 thin\", ylim=c(0,1000000), yaxt=\"n\")")
r("boxplot(n1stpram, main=\"1 greedy vs 1 thin\", ylim=c(0,1000000), yaxt=\"n\")")
r("axis(2, at=c(0, 100000, 200000, 300000, 400000, 500000, 600000, 700000, 800000, 900000, 1000000), labels=c(\"0\", \"100\", \"200\", \"300\", \"400\", \"500\", \"600\", \"700\", \"800\", \"900\", \"1000\"))")
r("par(mar=c(1,0,2,0), yaxt=\"n\")")
#r("boxplot(n2stpt, n2stpm, n2stpa, n2stpam, n2stpr, n2stpram, main=\"2 greedy vs 2 thin\", ylim=c(0,1000000))")
r("boxplot(n2stpram, main=\"2 greedy vs 2 thin\", ylim=c(0,1000000))")
#r("boxplot(n4stpt, n4stpm, n4stpa, n4stpam, n4stpr, n4stpram, main=\"4 greedy vs 4 thin\", ylim=c(0,1000000))")
#r("boxplot(n8stpt, n8stpm, n8stpa, n8stpam, n8stpr, n8stpram, main=\"8 greedy vs 8 thin\", ylim=c(0,1000000))")
#r("boxplot(n16stpt, n16stpm, n16stpa, n16stpam, n16stpr, n16stpram, main=\"16 greedy vs 16 thin\", ylim=c(0,1000000))")
r("par(mar=c(10,5,2,0), xaxt=\"s\", yaxt=\"s\")")
#r("boxplot(n32stpt, n32stpm, n32stpa, n32stpam, n32stpr, n32stpram, main=\"32 greedy vs 32 thin\", ylim=c(0,1000000), xaxt=\"n\", xlab=\"\", yaxt=\"n\")")
r("axis(2, at=c(0, 100000, 200000, 300000, 400000, 500000, 600000, 700000, 800000, 900000, 1000000), labels=c(\"0\", \"100\", \"200\", \"300\", \"400\", \"500\", \"600\", \"700\", \"800\", \"900\", \"1000\"))")
#r("text(1:6, par(\"usr\")[3] * offset, srt = 60, adj = 1, labels = labels, xpd = TRUE, cex=1.15)")
r("par(mar=c(10,0,2,0), yaxt=\"n\")")
#r("boxplot(n64stpt, n64stpm, n64stpa, n64stpam, n64stpr, n64stpram, main=\"64 greedy vs 64 thin\", ylim=c(0,1000000), xaxt=\"n\", xlab=\"\")")
#r("text(1:6, par(\"usr\")[3] * offset, srt = 60, adj = 1, labels = labels, xpd = TRUE, cex=1.15)")
#r("boxplot(n128stpt, n128stpm, n128stpa, n128stpam, n128stpr, n128stpram, main=\"128 greedy vs 128 thin\", ylim=c(0,1000000), xaxt=\"n\", xlab=\"\")")
#r("text(1:6, par(\"usr\")[3] * offset, srt = 60, adj = 1, labels = labels, xpd = TRUE, cex=1.15)")
#r("boxplot(n256stpt, n256stpm, n256stpa, n256stpam, n256stpr, n256stpram, main=\"256 greedy vs 256 thin\", ylim=c(0,1000000), xaxt=\"n\", xlab=\"\")")
#r("text(1:6, par(\"usr\")[3] * offset, srt = 60, adj = 1, labels = labels, xpd = TRUE, cex=1.15)")
#r("boxplot(n512stpt, n512stpm, n512stpa, n512stpam, n512stpr, n512stpram, main=\"512 greedy vs 512 thin\", ylim=c(0,1000000), xaxt=\"n\", xlab=\"\")")
#r("text(1:6, par(\"usr\")[3] * offset, srt = 60, adj = 1, labels = labels, xpd = TRUE, cex=1.15)")
r("mtext(\"Throughput (Kbit/second aggregated over 30 seconds)\", side = 2, line = -1.2, outer=TRUE, cex=0.95)")
r("mtext(\"TCP variation used for competing streams\", side = 1, line = -3, outer=TRUE, cex=0.95)")
r("dev.off()")
