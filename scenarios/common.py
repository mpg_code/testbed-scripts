#!/usr/bin/env python
#coding=utf8

from pxssh import pxssh
from re import match
import time
from threading import Timer


class timer(object):
	def __init__(self, duration, *hosts):
		self.remaining = len(hosts)

		def kill():
			print 'Stopping streams...'
			for host in hosts:
				host.kill()
				self.remaining -= 1

		self.kill = kill
		self.timer = Timer(duration * 1.0, kill)
		self.timer.start()

	def done(self):
		return self.remaining == 0

	def cancel(self):
		self.timer.cancel()
		self.kill()


class host(object):
	def __init__(self, addr, **kwargs):
		self.addr = str(addr)

		self.handle = pxssh(logfile=kwargs.get('log', None))
		self.wait = not kwargs.get('log', None) is None

		self.handle.login(addr, 'root')

		def factory(attr):
			return lambda x: x.ifaces[str(attr)]

		self.ifaces = dict()
		for name, iface in filter(lambda item: match(r'^(eth|br|wlan|wan|tun|lo|mon)[0-9]*$', item[0]), kwargs.items()):
			self.ifaces[str(name)] = iface
			iface.host = self
			setattr(self.__class__, name, property(factory(name)))

	def __str__(self):
		return self.addr

	def disable_offload(self, iface):
		print "Disabling TSO/GSO/GRO on iface %s on %s" % (iface, self)
		self.handle.sendline("ethtool -K %s gso off" % iface)
		while not self.handle.prompt():
			continue
		self.handle.sendline("ethtool -K %s gro off" % iface)
		while not self.handle.prompt():
			continue
		self.handle.sendline("ethtool -K %s tso off" % iface)
		while not self.handle.prompt():
			continue
		print "Setting txqueuelen on iface %s on %s" % (iface, self)
		self.handle.sendline("ifconfig %s txqueuelen 0" % iface)
		while not self.handle.prompt():
			continue

	def kill(self):
		self.handle.sendintr()
		if self.wait:
			try:
				time.sleep(5)
			except:
				pass

		self.handle.sendintr()
		while not self.handle.prompt():
			continue

		self.handle.terminate(force=True)
		self.handle.logout()


class iface(object):
	def __init__(self, cap=None):
		self.host = None
		self.bwcap = cap
		self.__cleared = False

	def clear(self):
		if self.host == None:
			raise Warning("Interface unbound")

		ssh = self.host.handle

		ssh.sendline("tc qdisc del dev %s root" % self)
		while not ssh.prompt():
			continue

		if self.bwcap != None:
			print "Capping bandwidth to %dkbit/s on %s %s" % (self.bwcap, self.host, self)
			#ssh.sendline("tc qdisc add dev %s root handle 1: tbf rate %dkbit burst 1542 limit 1542" % (self, self.bwcap))
			ssh.sendline("tc qdisc add dev %s root handle 1: tbf rate %dkbit burst 1542 limit 1542" % (self, self.bwcap))
			#ssh.sendline("tc qdisc add dev %s root handle 1: htb default 10" % self)
			#ssh.sendline("tc class add dev %s parent 1: classid 1:10 htb rate %dkbit ceil %dkbit burst 1542" % (self, self.bwcap, self.bwcap))

		self.__cleared = True

	def apply(self, qdisc, **params):
		if not self.__cleared:
			self.clear()

		ssh = self.host.handle

		print "Applying qdisc %s on %s %s" % (qdisc, self.host, self)
		if self.bwcap != None:
			#ssh.sendline("tc qdisc add dev %s parent 1: %s %s" % (self, qdisc, " ".join(str(param) + " " + str(value) for param, value in params.items())))
			ssh.sendline("tc qdisc add dev %s parent 1:10 %s %s" \
					% (self, qdisc, " ".join(str(param) + " " + str(value) for param, value in params.items())))
		else:
			ssh.sendline("tc qdisc add dev %s root %s %s" \
					% (self, qdisc, " ".join(str(param) + " " + str(value) for param, value in params.items())))

		while not ssh.prompt():
			continue

	def __str__(self):
		for name, iface in self.host.ifaces.items():
			if iface == self:
				return name
		raise Warning("Interface unbound")


class delay_iface(iface):

	def __init__(self):
		super(self.__class__, self).__init__(cap=None)
		self.__cleared = False
		self.bands = 3
		self.delays = {}

	def apply(self):
		if not self.__cleared:
			self.clear()

		ssh = self.host.handle

		# Set root level prio qdisc
		ssh.sendline("tc qdisc add dev %(iface)s root handle 1: prio bands %(bands)d priomap %(priomap)s"
				% {'iface': self, 'bands': self.bands, 'priomap': " 1"*16})
		while not ssh.prompt():
			continue


		# Set root level filter
		band = 0
		for port in self.delays.iterkeys():
			ssh.sendline("tc filter add dev %(iface)s parent 1: protocol ip prio 1 u32 match ip dport %(port)d 0xffff classid 1:%(band)d"
					% {'iface': self, 'band': band/self.bands + 1, 'port': port})
			while not ssh.prompt():
				continue

			band += 1

		# Set second level prio qdisc
		for band in xrange(1, self.bands+1):
			ssh.sendline("tc qdisc add dev %(iface)s parent 1:%(parent)d handle %(handle)d: prio bands %(bands)d priomap %(priomap)s"
					% {'iface': self, 'bands': self.bands, 'priomap':" 1"*16, 'parent': band, 'handle': 10*band})
			while not ssh.prompt():
				continue

		# Set second level filter and leaf level netem qdisc
		band = 0
		for port, delay in self.delays.iteritems():
			print "Setting %dms delay on %s for dport %d" % (delay, self, port)
			ssh.sendline("tc qdisc add dev %(iface)s parent %(parent)d:%(sub)d netem delay %(delay)dms limit 1000"
					% {'iface': self, 'parent': 10 * ((band/self.bands + 1)), 'sub': band % self.bands + 1, 'delay': delay})
			while not ssh.prompt():
				continue

			ssh.sendline("tc filter add dev %(iface)s parent %(parent)d: protocol ip prio 1 u32 match ip dport %(port)d 0xffff classid %(parent)d:%(sub)d"
					% {'iface': self, 'parent': 10 * ((band/self.bands + 1)), 'sub': band % self.bands + 1, 'port': port})
			while not ssh.prompt():
				continue

			band += 1


	def set_delay(self, port, delay):
		if len(self.delays) > 16**2:
			raise Warning("Exceeding maximum number of classes")

		self.delays[port] = delay

		if len(self.delays) > self.bands**2:
			self.bands += 1


class stream(object):
	def __init__(self, recv_addr, recv_port, **kwargs):
		attributes = {
				'recv_addr': str(recv_addr),
				'recv_port': int(recv_port),
				'start_port': int(kwargs.get('start_port', 15000)),
				'packet_interval': int(kwargs.get('packet_interval', 5)),
				'packet_size': int(kwargs.get('packet_size', 1460)),
				'stream_duration': int(kwargs.get('stream_duration', 15)),
				'stream_mechanism': str(kwargs.get('stream_mechanism', '')),
				'num_streams': int(kwargs.get('num_streams', 1)),
				'stream_interval': int(kwargs.get('stream_interval', 10)),
				'bandwidth': int(kwargs.get('bandwidth', 0))
				}

		def create_attr(attr_id, name):
			# create object property with getters and setters
			def getter(obj):
				return obj._attr[attr_id]

			def setter(obj, value):
				obj._attr[attr_id] = value

			setattr(self.__class__, name, property(getter, setter))

		for name in attributes.keys():
			create_attr(name, name)

		mapping = {
				'iat': 'packet_interval',
				'pival': 'packet_interval',
				'send_port': 'start_port',
				'ps': 'packet_size',
				'size': 'packet_size',
				'dur': 'stream_duration',
				'duration': 'stream_duration',
				'flags': 'stream_mechanism',
				'mechanism': 'stream_mechanism',
				'num': 'num_streams',
				'number': 'num_streams',
				'sival': 'stream_interval'
				}

		for name, real_name in mapping.items():
			if kwargs.has_key(name):
				attributes[real_name] = kwargs.get(name)
			create_attr(real_name, name)

		self._attr = attributes

	@property
	def client_string(self):
		if self.bandwidth > 0:
			print "Using bandwidth %d" % int(self.bandwidth)
			return "/root/streamzero_client" \
					+ " -v2" \
					+ " -s %s" % str(self.recv_addr) \
					+ " -p %d" % int(self.recv_port) \
					+ " -d %d" % int(self.duration) \
					+ " -c %d" % int(self.num_streams) \
					+ " -j %d" % int(self.stream_interval) \
					+ " -W" \
					+ " -P %d" % int(self.start_port) \
					+ " -b %d" % int(self.bandwidth) \
					+ " -n" \
					+ " " + str(self.mechanism)

		print "Sending %dB every %dms" % (int(self.packet_size), int(self.packet_interval))
		return "/root/streamzero_client" \
				+ " -v2" \
				+ " -s %s" % str(self.recv_addr) \
				+ " -p %d" % int(self.recv_port) \
				+ " -d %d" % int(self.duration) \
				+ " -c %d" % int(self.num_streams) \
				+ " -W" \
				+ " -P %d" % int(self.start_port) \
				+ " -i %d" % int(self.packet_interval) \
				+ " -S %d" % int(self.packet_size) \
				+ " -n" \
				+ " " + str(self.mechanism)

	@property
	def server_string(self):
		return "/root/streamzero_srv" \
				+ " -v2" \
				+ " -n %d" % int(self.num_streams) \
				+ " -q" \
				+ " -p %d" % int(self.recv_port) \
				+ " -r 2" \
				+ " -S"
