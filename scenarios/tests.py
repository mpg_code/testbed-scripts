#!/usr/bin/env python
#coding=utf8

from pxssh import pxssh as _pxssh
import time
from os.path import join as _path
from threading import Timer
from math import ceil
from os import system
from sys import argv, exit
import itertools


_shortnames = {
		'congestion_control': 'cc',
		'sack': 'sack',
		'dsack': 'dsack',
		'window_scaling': 'winscale',
		'retrans_collapse': 'retrclps',
		'fack': 'fack',
		'early_retrans': 'earlretr',
		'low_latency': 'lolat',
		'thin_dupack': 'thinDA',
		'thin_linear_timeouts': 'thinLT'
		}


def _sleep(seconds):
	try:
		time.sleep(seconds)
		return True
	except:
		return False

def _wait(ssh_handle):
	while not ssh_handle.prompt():
		continue


class host(object):
	def __init__(self, addr, logfile, **ifaces):
		self.addr = addr
		self.handle = _pxssh(logfile=logfile)
		self.handle.login(addr, 'root')
		self.ifaces = {}
		self.__active = True

		def factory(name):
			return lambda x: x.ifaces[name]

		for name, iface in ifaces.iteritems():
			print "Mapping iface %s on %s" % (name, self)
			self.ifaces[name] = iface
			iface._host = self
			iface.clear()
			setattr(self.__class__, name, property(factory(name)))

	def disable_offload(self):
		for name, iface in self.ifaces.iteritems():
			iface.disable_offload()

	def kill(self):
		if not self.__active:
			return

		self.handle.sendintr()
		_sleep(5)
		self.handle.sendintr()
		_wait(self.handle)

		self.handle.terminate(force=True)
		self.handle.logout()
		self.__active = False

	def __str__(self):
		return self.addr


class iface(object):
	def __init__(self):
		self._host = None

	def disable_offload(self):
		ssh = self._host.handle
		iface = str(self)

		print "Disabling offloading on %s %s" % (self._host, iface)
		for t in ['gso', 'gro', 'tso', 'lro']:
			ssh.sendline("ethtool -K %s %s off" % (iface, t))
			_wait(ssh)

		ssh.sendline("ifconfig %s txqueuelen 0" % iface)
		_wait(ssh)

	def clear(self):
		ssh = self._host.handle

		ssh.sendline("tc qdisc del dev %s root" % self)
		_wait(ssh)

	def __str__(self):
		for name, iface in self._host.ifaces.iteritems():
			if iface == self:
				return name

		raise Warning("Interface unbound")

	def apply(self, qdisc, **args):
		ssh = self._host.handle
		iface = str(self)

		print "Setting qdisc %s on %s %s" % (qdisc, self._host, iface)

		ssh.sendline("ethtool -s %s duplex full" % iface)
		ssh.sendline("ethtool -r %s" % iface)
		ssh.sendline("tc qdisc add dev %s root %s %s"
				% (iface, qdisc, " ".join(str(param) + " " + str(value) for param, value in args.iteritems())))

		_wait(ssh)


class nic_iface(iface):
	def __init__(self, rate):
		super(nic_iface, self).__init__()
		self.rate = rate

	def apply(self, qdisc, **args):
		ssh = self._host.handle
		iface = str(self)

		print "Setting qdisc %s on %s %s (NIC bottleneck, 10mbps)"
		ssh.sendline("ethtool -s %s speed 10 duplex full autoneg off advertise 0x002" % iface)
		ssh.sendline("tc qdisc add dev %s root %s %s"
				% (iface, qdisc, " ".join(str(param) + " " + str(value) for param, value in args.iteritems())))

		_wait(ssh)

class tbf_iface(iface):
	def __init__(self, rate):
		super(tbf_iface, self).__init__()
		self.rate = rate

	def apply(self, qdisc, **args):
		ssh = self._host.handle
		iface = str(self)

		print "Setting qdisc %s on %s %s (tbf bottleneck)" % (qdisc, self._host, iface)

		ssh.sendline("ethtool -s %s speed 1000 duplex full autoneg on advertise 0x020" % iface)
		ssh.sendline("ethtool -r %s" % iface)
		ssh.sendline("tc qdisc add dev %s root handle 1: tbf rate %dkbit burst 1520 limit 1520" % (iface, self.rate))
		ssh.sendline("tc qdisc add dev %s parent 1: %s %s"
				% (iface, qdisc, " ".join(str(param) + " " + str(value) for param, value in args.iteritems())))

		_wait(ssh)


class htb_iface(iface):
	def __init__(self, rate):
		super(htb_iface, self).__init__()
		self.rate = rate

	def apply(self, qdisc, **args):
		ssh = self._host.handle
		iface = str(self)

		print "Setting qdisc %s on %s %s (htb bottleneck)" % (qdisc, self._host, iface)

		ssh.sendline("ethtool -s %s speed 1000 duplex full autoneg on advertise 0x020" % iface)
		ssh.sendline("ethtool -r %s" % iface)
		ssh.sendline("tc qdisc add dev %s root handle 1: htb default 10" % iface)
		ssh.sendline("tc class add dev %s parent 1: classid 1:10 htb rate %dkbit ceil %dkbit burst 1520" % (iface, self.rate, self.rate))
		ssh.sendline("tc qdisc add dev %s parent 1:10 %s %s"
				% (iface, qdisc, " ".join(str(param) + " " + str(value) for param, value in args.iteritems())))

		_wait(ssh)


class streamclass(object):
	def __init__(self, name, send_addr, recv_addr, recv_port, **kwargs):
		self.name = name
		self.send_addr = send_addr
		self.recv_addr = recv_addr
		self.recv_port = recv_port
		self.start_port = kwargs.get('start_port', 15000)
		self.packet_intertransmission_time = kwargs.get('packet_itt', 5)
		self.packet_intertransmission_time_var = kwargs.get('packet_itt_var', 0)
		self.packet_size = kwargs.get('packet_size', 1448)
		self.packet_size_var = kwargs.get('packet_size_var', 0)
		self.ramp_up_time = kwargs.get('ramp_up_time', 100)
		self.ramp_up_time_var = kwargs.get('ramp_up_time_var', 0)
		self.bandwidth = kwargs.get('bandwidth', 0)
		self.mechanism = kwargs.get('mechanism', '')
		self.duration = kwargs.get('duration', 3600)
		self.number = kwargs.get('number', 1)
		self.span_ports = kwargs.get('span_ports', False)
		self.algorithm = kwargs.get('algorithm', "cubic")

	@property
	def config(self):
		if self.bandwidth > 0:
			return {
					'name': self.name,
					'send_addr': self.send_addr,
					'recv_addr': self.recv_addr,
					'recv_port': self.recv_port,
					'start_port': self.start_port,
					'ramp_up': self.ramp_up_time,
					'ramp_up_var': self.ramp_up_time_var,
					'ramp_up_str': "%d:%d" % (self.ramp_up_time, self.ramp_up_time_var),
					'bandwidth': self.bandwidth,
					'number': self.number,
					'span_ports': self.span_ports,
					'algorithm': self.algorithm
					}
		else:
			return {
					'name': self.name,
					'send_addr': self.send_addr,
					'recv_addr': self.recv_addr,
					'recv_port': self.recv_port,
					'start_port': self.start_port,
					'ramp_up': self.ramp_up_time,
					'ramp_up_var': self.ramp_up_time_var,
					'ramp_up_str': "%d:%d" % (self.ramp_up_time, self.ramp_up_time_var),
					'packet_itt': self.packet_intertransmission_time,
					'packet_itt_var': self.packet_intertransmission_time_var,
					'packet_itt_str': "%d:%d" % (self.packet_intertransmission_time, self.packet_intertransmission_time_var),
					'packet_size': self.packet_size,
					'packet_size_var': self.packet_size_var,
					'packet_size_str': "%d:%d" % (self.packet_size, self.packet_size_var),
					'mechanisms': self.mechanism,
					'number': self.number,
					'span_ports': self.span_ports,
					'algorithm': self.algorithm
					}

	@property
	def prefix(self):
		if self.bandwidth > 0:
			return "%(host)s-port_%(port)d-greedy_%(number)d-cc_%(algorithm)s-rampup_%(ramp_up)sms-rate_%(bandwidth)dkbit" % {
					'host': self.name,
					'port': self.start_port,
					'number': self.number,
					'ramp_up': "%d:%d" % (self.ramp_up_time, self.ramp_up_time_var),
					'bandwidth': self.bandwidth,
					'algorithm': self.algorithm
					}
		else:
			return "%(host)s-port_%(port)d-thin_%(number)d-cc_%(algorithm)s-rampup_%(ramp_up)sms-ps_%(packet_size)sB-itt_%(packet_itt)sms-%(mechs)s" % {
					'host': self.name,
					'port': self.start_port,
					'number': self.number,
					'ramp_up': "%d:%d" % (self.ramp_up_time, self.ramp_up_time_var),
					'packet_itt': "%d:%d" % (self.packet_intertransmission_time, self.packet_intertransmission_time_var),
					'packet_size': "%d:%d" % (self.packet_size, self.packet_size_var),
					'mechs': self.mechanism,
					'algorithm': self.algorithm
					}

	@property
	def client_string(self):
		string = "/root/streamzero_client" \
				+ " -v2" \
				+ " -d %d" % self.duration \
				+ " -s %s" % self.recv_addr \
				+ " -p %d" % self.recv_port \
				+ " -c %d" % self.number \
				+ " -j %d:%d" % (self.ramp_up_time, self.ramp_up_time_var) \
				+ " -W" \
				+ " -P %d" % self.start_port \
				+ " -G %s" % self.algorithm \
				+ (" -n" if self.span_ports else "")

		if self.bandwidth > 0:
			string += " -b %d" % self.bandwidth
		else:
			string += " -i %d:%d" % (self.packet_intertransmission_time, self.packet_intertransmission_time_var) \
					+ " -S %d:%d" % (self.packet_size, self.packet_size_var) \
					+ " " + self.mechanism

		return string

	@property
	def server_string(self):
		return "/root/streamzero_srv" \
				+ " -v2" \
				+ (" -n %d" % self.number if self.span_ports else "") \
				+ " -q" \
				+ " -p %d" % self.recv_port \
				+ " -r 2" \
				+ " -S"


class test(object):
	def __init__(self, test_directory, test_number, prefix, test_config, bottleneck_iface):
		print "INIT test %d..." % test_number
		self.test_dir = test_directory
		self.test_num = test_number
		self.test_cfg = test_config
		self._prefix = prefix

		self.monitor_after = host('172.16.0.32', logfile=None, eth1=iface(), eth2=iface())
		self.monitor_after.disable_offload()
		self.monitors = []

		self.bridge_before = host('172.16.0.31',
				logfile=open(_path(self.test_dir, self.prefix + "_before_ssh.log"), "w"),
				eth1=iface(),
				eth2=iface(),
				eth3=iface(),
				eth4=iface()
				)
		self.bridge_after = host('172.16.0.32',
				logfile=open(_path(self.test_dir, self.prefix + "_after_ssh.log"), "w"),
				eth1=iface(),
				eth2=iface()
				)
		self.bridge_before.disable_offload()
		self.bridge_after.disable_offload()

		self.router = host('172.16.0.30',
				logfile=open(_path(self.test_dir, self.prefix + "_router_ssh.log"), "w"),
				eth1=iface(),
				eth2=bottleneck_iface(test_config['bw'])
				)
		self.router.disable_offload()

		self.bdp_bytes = int((test_config['bw'] * 10**3) * (test_config['rtt'] * 10**-3) / 8.0)
		self.bdp_packets = int(ceil(self.bdp_bytes / 1514.0))
		print "bandwidth:%dkbit * rtt:%dms = bdp:%dB (~%dp á 1514B)" % (test_config['bw'], test_config['rtt'], self.bdp_bytes, self.bdp_packets)

		self.streams = []
		self.connections = []
		self.files = []

		self._configure_netem()
		self._configure_router()

	@property
	def prefix(self):
		cfg = self.test_cfg
		prefix = "%(prefix)s-%(test_num)d-dur_%(dur)ds-bw_%(bw)dkbit-rtt_%(rtt)sms-queue_%(queue)s" % {
				'prefix': self._prefix,
				'test_num': self.test_num,
				'dur': cfg['duration'],
				'bw': cfg['bw'],
				'rtt': cfg['rtt'],
				'queue': cfg['router']['name'],
				}

		for opt, val in cfg['tcp'].iteritems():
			prefix += "-%s%s" % (_shortnames[opt], str(val))

		return prefix

	def add_stream(self, send_addr, recv_addr, stream):
		stream.duration = self.test_cfg['duration']
		self.streams.append((send_addr, recv_addr, stream))

	def _configure_netem(self):
		cfg = self.test_cfg
		self.bridge_after.eth1.apply('netem', delay='%dms' % (cfg['rtt'] / 2), limit=self.bdp_packets * 2)
		self.bridge_after.eth2.apply('netem', delay='%dms' % (cfg['rtt'] / 2), limit=self.bdp_packets * 2)

	def _configure_router(self):
		cfg = self.test_cfg
		self.router.eth1.apply(cfg['router']['qdisc'], **cfg['router']['args'])
		self.router.eth2.apply(cfg['router']['qdisc'], **cfg['router']['args'])

	@staticmethod
	def _set_options(handle, options):
		print "Setting TCP options..."
		handle.sendline("sysctl -w net.ipv4.tcp_no_metrics_save=1")
		_wait(handle)
		handle.sendline("sysctl -w net.ipv4.tcp_timestamps=1")
		_wait(handle)

		for option, value in options.iteritems():
			handle.sendline("sysctl -w net.ipv4.tcp_%s=%s" % (str(option), str(value)))
			_wait(handle)

	def _start_receivers(self):
		print "Monitoring receiver-side on %s" % (self.monitor_after.addr)
		self.monitor_after.handle.sendline("tcpdump -Nn -i br0 -s 100 -w /root/%s_after.dump tcp &> /root/%s_after.log" % (self.prefix, self.prefix))
		self.files.append((self.monitor_after.addr, '%s_after.dump' % self.prefix))
		self.files.append((self.monitor_after.addr, '%s_after.log' % self.prefix))

		print "Starting receivers..."
		for send_addr, recv_addr, stream in self.streams:
			prefix = "%s-%s" % (self.prefix, stream.prefix)

			print "Starting receiver on %s on port %d-%d for %d connections" % (stream.recv_addr, stream.recv_port, (stream.recv_port+stream.number), stream.number)
			h = host(recv_addr, logfile=open(_path(self.test_dir, "%s_receiver.log" % prefix), "w"))
			test._set_options(h.handle, {'sack': 1, 'dsack': 1, 'fack': 1, 'window_scaling': 1, 'retrans_collapse': 1})
			h.handle.sendline(stream.server_string)

			self.connections.append(h)

	def _start_senders(self, monitor_on_sender):
		print "Starting senders..."
		for send_addr, recv_addr, stream in self.streams:
			filterstr = '"tcp and host %s and portrange %d-%d"' % (stream.recv_addr, stream.start_port, (stream.start_port + stream.number))
			prefix = "%s-%s" % (self.prefix, stream.prefix)

			m_addr = send_addr if monitor_on_sender else '172.16.0.31'

			print "Monitoring sender-side on %s" % m_addr
			monitor = host(m_addr, logfile=None, eth1=iface(), eth2=iface())
			monitor.eth1.disable_offload()
			self.monitors.append(monitor)
			monitor.handle.sendline("tcpdump -Nn -i eth1 -s 100 -w /root/%s_front.dump %s &> /root/%s_front.log" % (prefix, filterstr, prefix))
			self.files.append((m_addr, '%s_front.dump' % prefix))
			self.files.append((m_addr, '%s_front.log' % prefix))

			print "Starting sender on %s on port %d-%d for %d connections" % (send_addr, stream.start_port, (stream.start_port+stream.number), stream.number)
			h = host(send_addr, logfile=open(_path(self.test_dir, "%s_sender.log" % prefix), "w"))
			test._set_options(h.handle, self.test_cfg['tcp'])
			h.handle.sendline(stream.client_string)

			self.connections.append(h)

	def _stop(self):
		print "Copying files..."
		for conn in self.connections:
			conn.kill()

		self.monitor_after.kill()
		for monitor in self.monitors:
			monitor.kill()

		self.bridge_after.kill()
		self.bridge_before.kill()
		self.router.kill()

		for host, filename in self.files:
			system("scp root@%(host)s:/root/%(filename)s %(out)s && ssh %(host)s rm -f /root/%(filename)s" % {'host': host, 'filename': filename, 'out': _path(self.test_dir, filename)})

	def run(self, monitor_on_sender=False):
		print "START test %d" % (self.test_num)
		self._start_receivers()
		_sleep(5)
		self._start_senders(monitor_on_sender)

		duration = self.test_cfg['duration']

		proc = _timer(duration, *self.connections)
		print "RUNNING for %d minutes %d seconds..." % (duration / 60, duration % 60)
		while not proc.done():
			if not	_sleep(3):
				break

			for h in self.connections:
				try:
					h.handle.read_nonblocking(size=1000, timeout=0)
				except:
					pass

		if proc.done():
			print "SUCCESS!!"
		else:
			print proc.cancel()
			print "ABORTED!!"



		print "Resetting..."
		_sleep(10)
		self._stop()

		print "Dumping config..."
		with open(_path(self.test_dir, self._prefix + "_config.txt"), "a") as f:
			config = {
					'test': self.test_num,
					'config': self.test_cfg,
					'streams': [{
						'config': stream.config,
						'results': ('%s-%s_front.dump' % (self.prefix, stream.prefix), '%s_after.dump' % self.prefix)
						} for s, r, stream in self.streams]
					}
			f.write(str(config) + "\n")

		print "END test %d" % (self.test_num)


class _timer(object):
	def __init__(self, duration, *hosts):
		self.remaining = len(hosts)

		def kill_them_all_johnny():
			print "Stopping streams..."
			for host in hosts:
				host.kill()
				self.remaining -= 1

		self.kill = kill_them_all_johnny
		self.timer = Timer(duration * 1.0, kill_them_all_johnny)
		self.timer.start()

	def done(self):
		return self.remaining == 0

	def cancel(self):
		self.timer.cancel()
		self.kill()


if __name__ == "__main__":
	repetitions = 1				# number of times to repeat everything
	bandwidth = 5000			# bandwidth in kilobits
	bottleneck = htb_iface		# which method of bandwidth limitation to use
	one_way_delay = 75			# one-way delay in milliseconds (RTT = 2*one_way_delay)
	num_greedy_streams = [10]	# number of greedy streams
	total_duration = 300		# test duration
	ramp_up_greedy = 100		# ramp-up time for greedy streams (time between each stream start)
	algorithm = "reno"			# TCP algorithm for greedy streams

	# thin stream settings
	# (number of streams, ramp up time, packet size, intertransmission time, TCP algorithm, mechanisms)
	thin_settings = [
			[(10, "100:0", "80:0", "75:0", "reno", ""), (10, "100:0", "400:0", "120:0", "reno", ""), (10, "100:0", "100:0", "100:0", "reno", "")],
			[(10, "100:0", "80:0", "75:0", "reno", ""), (10, "100:0", "400:0", "120:0", "reno", ""), (10, "100:0", "100:0", "100:15", "reno", "")],
			]

	bdp_bytes = int((bandwidth * 10**3) * ((2 * one_way_delay) * 10**-3) / 8.0)
	bdp_packets = int(ceil(bdp_bytes / 1514.0))

	queues = [
			('short_pfifo', 'pfifo', {'limit': bdp_packets/2}),
			#('short_bfifo', 'bfifo', {'limit': bdp_bytes/2})
			#('medium', 'pfifo', {'limit': bdp_packets})
			]

	# TCP options for the sender
	tcp_opts = [
			{
				#'congestion_control': 'reno',
				'sack': 1,
				'dsack': 1,
				'fack': 1,
				'window_scaling': 1,
				'retrans_collapse': 1,
				'low_latency': 0,
				'early_retrans': 2,
				}
			]

	try:
		directory = argv[1]
	except:
		print "Usage: %s <output directory>" % argv[0]
		exit(1)

	system("mkdir -p %s" % directory)


	ts = time.strftime("%Y%m%d_%H%M%S")

	test_scenarios = [i for i in itertools.product(num_greedy_streams, queues, tcp_opts, thin_settings)] * repetitions
	test_num = 0

	for num_greedy, router, tcp_opt, thin_opts in test_scenarios:
		print "Starting test %d of %d" % ((test_num+1), len(test_scenarios))

		# Create test scenario
		t = test(directory, test_num, ts, {
			'bw': bandwidth,
			'rtt': 2 * one_way_delay,
			'duration': total_duration,
			'tcp': tcp_opt,
			'router': {'name': router[0], 'qdisc': router[1], 'args': router[2]}
			}, bottleneck_iface=bottleneck)

		# Add cross-traffic
		t.add_stream('172.16.0.21', '172.16.0.20', streamclass(
			number=num_greedy,
			name='%dgreedy' % num_greedy,
			send_addr='10.0.0.11',
			recv_addr='10.0.1.10',
			recv_port=12345,
			start_port=15000,
			bandwidth=bandwidth,
			ramp_up_time=ramp_up_greedy,
			ramp_up_time_var=0,
			duration=total_duration,
			algorithm=algorithm
			))

		# Configure thin-stream senders
		thin_ports = (25000, 20000)
		for num_thin, ramp_up, ps, itt, alg, mech in thin_opts:

			ramp_up_t, ramp_up_v = ramp_up.split(":")
			ps_s, ps_v = ps.split(":")
			itt_t, itt_v = itt.split(":")


			t.add_stream('172.16.0.10', '172.16.0.20', streamclass(
				number=num_thin,
				name='%dthin_rampup%s_ps%s_itt%s_cc%s%s' % (num_thin, ramp_up, ps, itt, alg, mech),
				send_addr='10.0.0.10',
				recv_addr='10.0.1.10',
				recv_port=thin_ports[0],
				start_port=thin_ports[1],
				ramp_up_time=int(ramp_up_t),
				ramp_up_time_var=int(ramp_up_v),
				packet_size=int(ps_s),
				packet_size_var=int(ps_v),
				packet_itt=int(itt_t),
				packet_itt_var=int(itt_v),
				mechanism=mech,
				duration=total_duration,
				algorithm=alg
				))

			thin_ports = (thin_ports[0] + num_thin + 1, thin_ports[1] + num_thin + 1)


		t.run(monitor_on_sender=False)
		test_num += 1

	print "ALL DONE"
