#!/usr/bin/env python
#coding=utf8

from math import ceil
from common import *
from time import sleep
from os import system
from os.path import join as path
from sys import argv, exit



### Test parameters ###
bandwidth = 5000			# bandwidth in kilobits
one_way_delay = 75			# one way delay in milliseconds (RTT=2*one_way_delay)

#num_streams = [30, 25, 20, 15, 10, 5, 2, 1]
num_streams = [10, 20, 30]
num_greedy = 10
#mechanisms = [ "", "-a", "-m", "-am" ]
mechanisms = [ "" ]

#algorithms = [ 'reno', 'cubic' ]
algorithms = ['cubic']

total_duration = 300		# total test duration
stream_interval = 100		# stream ramp-up interval
thin_ps = 120				# packet size
thin_itt = 100				# inter-transmission time

#use_variable_delays = False

bwd_bytes = int((bandwidth * 10**3) * ((2*one_way_delay) * 10**-3) / 8.0)
bwd_packets = int(ceil(bwd_bytes / 1514.0))  # should be 62

queues = [
		('short', 'pfifo', {'limit': bwd_packets/2}),
		('medium', 'pfifo', {'limit': bwd_packets}),
		('long', 'pfifo', {'limit': bwd_packets*2})
		]

sender_settings = [
		[
			"ifconfig eth1 txqueuelen 1000",
			"sysctl -w net.ipv4.tcp_no_metrics_save=1",
			"sysctl -w net.ipv4.tcp_window_scaling=1",
			"sysctl -w net.ipv4.tcp_retrans_collapse=0",
		],
		[
			"ifconfig eth1 txqueuelen 1000",
			"sysctl -w net.ipv4.tcp_no_metrics_save=1",
			"sysctl -w net.ipv4.tcp_window_scaling=1",
			"sysctl -w net.ipv4.tcp_retrans_collapse=1"
		]
]

#queues = dict(
##		bfifo = {'limit': bwd_bytes}, # buffer size = RTT * C (Bw*Delay)
#		pfifo = {'limit': bwd_packets}, # same as above, rounded up to nearest packet
##		sfq = {'perturb': 10}, # advised perturb value
##		sfb = {'limit': bwd_packets},  # should be somewhat self-adjusting
##		qfq = {},
##		codel = {},  # we're assuming that codel is self configuring
##		fq_codel = {}, # this too
##		red = {'limit': bwd_bytes, 'avpkt': 1000}, # 1000 is a 'good value' according to the man page # TODO: calculate average packet size based on number of greedy packets (1514B) and number of thin packets (120)
##		choke = {'limit': bwd_packets, 'bandwidth': bandwidth} # same parameters as red, except it takes bandwidth
#		)


### End ###

#def frange(start, stop, step):
#	r = start
#	while r <= stop:
#		yield r
#		r += step


# Set variable delays
#min_delay = int(one_way_delay - one_way_delay * .10)
#max_delay = int(one_way_delay + one_way_delay * .10)

#delay_range = [i for i in frange(min_delay, max_delay, (max_delay-min_delay)/float(max(num_streams)))]

#delays = []
#for i in xrange(0, len(delay_range)/2):
#	delays.append((int(delay_range[i]), one_way_delay))
#	delays.append((int(delay_range[len(delay_range)-1-i]), one_way_delay))


# create output directory
try:
	output_dir = argv[1]
	system("mkdir -p %s" % output_dir)
except Exception as e:
	print "Usage: %s <output dir>" % argv[0]
	exit(1)


# streamzero addresses (sender, receiver)
client, server = '172.16.0.10', '172.16.0.20'
monitor_before_ip, monitor_before_iface = '172.16.0.31', 'br0'
monitor_after_ip, monitor_after_iface = '172.16.0.32', 'br0'

# emulate a large network with high rtt
bridge_before = host('172.16.0.31', eth1=iface(), eth2=iface())
bridge_before.eth1.clear()
bridge_before.eth2.clear()

bridge_after = None
#if not use_variable_delays:
if True:
	print "Using constant RTT, %d ms" % (2 * one_way_delay)
	bridge_after = host('172.16.0.32', eth1=iface(), eth2=iface())
	bridge_after.eth1.clear()
	bridge_after.eth2.clear()
	bridge_after.eth1.apply('netem', delay='%dms'%one_way_delay, limit=bwd_packets*2)
	bridge_after.eth2.apply('netem', delay='%dms'%one_way_delay, limit=bwd_packets*2)
else:
	print "Variable delay has been commented out"
	exit(1)

#else:
#	print "Using variable RTT"
#	bridge_after = host('172.16.0.32', eth1=delay_iface(), eth2=delay_iface())
#	bridge_after.eth1.clear()
#	bridge_after.eth2.clear()

#	for port, delay in enumerate(delays):
#		greedy_delay, thin_delay = delay
#		bridge_after.eth2.set_delay(12345+port, greedy_delay)
#		bridge_after.eth2.set_delay(23456+port, thin_delay)
#		bridge_after.eth1.set_delay(15000+port, greedy_delay)
#		bridge_after.eth1.set_delay(20000+port, thin_delay)
#
#	bridge_after.eth1.apply()
#	bridge_after.eth2.apply()

# disable TCP segmentation offloading
for interface in 1, 2, 3, 4:
	bridge_before.disable_offload('eth%d' % interface)
	bridge_after.disable_offload('eth%d' % interface)

bridge_after.kill()
bridge_before.kill()


def run_test():
	pass


# run tests
num_tests = len(algorithms) * len(queues)  * len(num_streams) * len(mechanisms) * len(sender_settings)
testno = 0
for queue_name, queue, queue_args in queues:

	# initialize bottlenecked router
	router = host('172.16.0.30', log=open(path(output_dir, "%s_%s_router.log" % (queue_name, queue)), "w"), eth2=iface(bandwidth), eth1=iface())
	router.eth1.apply(queue, **queue_args)
	router.eth2.apply(queue, **queue_args)
	router.disable_offload("eth1")
	router.disable_offload("eth2")
	router.kill()

	for algorithm in algorithms:
		for num in num_streams:
			for mech in mechanisms:
				for settings_idx, settings in enumerate(sender_settings):

					testno += 1
					print "Initializing new test (%d of %d)..." % (testno, num_tests)

					greedy = stream(recv_addr='10.0.1.10',
							num_streams=num_greedy,
							recv_port=12345,
							start_port=15000,
							bandwidth=bandwidth,
							stream_interval=stream_interval,
							congestion_control=algorithm,
							duration=total_duration)

					thin = stream(recv_addr='10.0.1.10',
							num_streams=num,
							flags=mech,
							recv_port=23456,
							start_port=20000,
							packet_size=thin_ps,
							packet_interval=thin_itt,
							stream_interval=stream_interval,
							congestion_control=algorithm,
							duration=total_duration)

					prefix = "fair-stable-%(test_number)d-%(duration)ssecs-%(rate)dkbit-rtt%(rtt)d-tcp_%(tcp)s-opts%(sender_opts)d-%(queue_name)s-%(thin)dvs%(greedy)d-itt%(itt)d-ps%(ps)d-%(mechanism)s" % \
							dict(
									test_number=testno, duration=total_duration, rate=bandwidth, rtt=2*one_way_delay, tcp=algorithm,
									queue_name=queue_name, queue_type=queue, thin=num, greedy=num_greedy, itt=thin_itt, ps=thin_ps, mechanism=mech, sender_opts=settings_idx+1
								)

					print "Configuring TCP algorithm (%s) on %s" % (algorithm, client)
					sender = host(client, log=open(path(output_dir, "%s_options_sender.log" % prefix), "w"))

					sender.handle.sendline("sysctl -w net.ipv4.tcp_congestion_control=%s" % algorithm)
					while not sender.handle.prompt():
						continue

					print "Configuring settings on %s" % client
					for setting in settings:
						sender.handle.sendline(setting)
						while not sender.handle.prompt():
							continue

					sender.kill()


					print "Starting streamzero servers on %s (log: %s_receiver_{greedy|thin}.log)" % (server, prefix)
					receiver_greedy = host(server, log=open(path(output_dir, "%s_receiver_greedy.log" % prefix), "w"))
					receiver_thin = host(server, doprint=True, log=open(path(output_dir, "%s_receiver_thin.log" % prefix), "w"))
					receiver_greedy.handle.sendline(greedy.server_string)
					receiver_thin.handle.sendline(thin.server_string)


					print "Starting tcpdump on %s %s (filename: %s_front.dump)" % (monitor_before_ip, monitor_before_iface, prefix)
					monitor_before = host(monitor_before_ip)
					monitor_before.handle.sendline("tcpdump -Nn -i %s -s 100 -w /root/%s_front.dump tcp &> /root/%s_front.log" % (monitor_before_iface, prefix, prefix))

					print "Starting tcpdump on %s %s (filename: %s_after.dump)" % (monitor_after_ip, monitor_after_iface, prefix)
					monitor_after = host(monitor_after_ip)
					monitor_after.handle.sendline("tcpdump -Nn -i %s -s 100 -w /root/%s_after.dump tcp &> /root/%s_after.log" % (monitor_after_iface, prefix, prefix))

					sleep(10)

					print "Starting streamzero clients on %s (log: %s_sender_{greedy|thin}.log)" % (client, prefix)
					sender_thin = host(client, log=open(path(output_dir, "%s_sender_thin.log" % prefix), "w"))
					sender_greedy = host(client, log=open(path(output_dir, "%s_sender_greedy.log" % prefix), "w"))
					sender_thin.handle.sendline(thin.client_string)
					sender_greedy.handle.sendline(greedy.client_string)

					proc = timer(total_duration, sender_thin, sender_greedy, receiver_thin, receiver_greedy)
					print "RUNNING for %d minutes %d seconds..." % (total_duration / 60, total_duration % 60)
					while not proc.done():
						try:
							sleep(5)
						except:
							break

						for h in sender_greedy, sender_thin, receiver_greedy, receiver_thin:
							try:
								h.read_nonblocking(size=1000, timeout=0)
							except:
								pass

					if proc.done():
						print "SUCCESS!!"
					else:
						proc.cancel()
						print "ABORTED!!"

					print "Resetting..."
					try:
						sleep(300)
					except:
						pass

					print "Killing tcpdump and copying dump-file..."
					monitor_after.kill()
					monitor_before.kill()

					if system("scp root@%s:/root/%s_front.{dump,log} %s && ssh %s rm -f /root/%s_front.{dump,log}" % (monitor_before_ip, prefix, output_dir, monitor_before_ip, prefix)) != 0:
						print "ERROR!"
						exit(1)
					if system("scp root@%s:/root/%s_after.{dump,log} %s && ssh %s rm -f /root/%s_after.{dump,log}" % (monitor_after_ip, prefix, output_dir, monitor_after_ip, prefix)) != 0:
						print "ERROR!"
						exit(1)


print "DONE"
